const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');

describe(__dirname, () => {

  it('works', () => {

    const scenes = 'splash,bounce,video,greeting,table,backgroundMusic'.split(',');
    const arrangement = [
      [
        'splash',
        [
          'bounce',
          'video',
          'greeting!',
        ],
        'table!',
      ],
      '!backgroundMusic',
    ];

    const result = proxyquire('.', {
      '__WEB_APP_DIR__/scenes': arrangement,
      ...scenes.reduce((scenes, s) => ({
        ...scenes,
        [`__WEB_APP_DIR__/scenes/${s}/component`]: {},
        [`__WEB_APP_DIR__/scenes/${s}/slides`]: [],
      }), {}),
    });

    assert.deepEqual(result, [
      [{ scene: 'splash', component: {}, slides: {} },
        [{ scene: 'bounce', component: {}, slides: {} },
          { scene: 'video', component: {}, slides: {} },
          {
            endSiblings: true,
            scene: 'greeting',
            component: {},
            slides: {}
          }
        ],
        { endSiblings: true, scene: 'table', component: {}, slides: {} }
      ],
      {
        endWithOthers: true,
        scene: 'backgroundMusic',
        component: {},
        slides: {}
      }
    ]);
  });

});

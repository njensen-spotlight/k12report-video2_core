const { walkScenes } = require('../utils');
const { merge } = require('lodash');

let scenes;
try {
  scenes = require(__WEB_APP_DIR__ + '/scenes');
} catch (error) {
  error.stack = 'Could not load scenes. ' + error.stack;
  error.message = 'Could not load scenes. ' + error.message;
  throw error;
}

scenes = walkScenes(scenes, req);

if (window.PUPPETEER_videoResult) {
  merge(scenes, window.PUPPETEER_videoResult.scenes);
}

module.exports = scenes;

function req(scene) {
  if (scene.component) {
    return scene;
  } else if (typeof scene !== 'string') {
    console.error(scene);
    throw new Error('Expected a scene string');
  }

  const module = {};

  if (scene.endsWith('!!')) {
    scene = scene.substr(0, scene.length - 2);
    module.endAll = true;
  }
  if (scene.endsWith('!')) {
    scene = scene.substr(0, scene.length - 1);
    module.endSiblings = true;
  }
  if (scene.startsWith('!')) {
    scene = scene.substr(1);
    module.endWithOthers = true;
  }

  module.scene = scene;
  module.component = require(__WEB_APP_DIR__ + '/scenes/' + scene + '/component');
  module.slides = require(__WEB_APP_DIR__ + '/scenes/' + scene + '/slides');
  // module.slides.splice(2, 1000);

  return module;
}


/* For Debugging Purposes */
// console.log('scenes:', scenes);
// bisect(scenes[0]);
// bisect(scenes[0], false);

/**
 * Splices (changes in place) first half (default) or second half of an array
 */
function bisect(array = [], firstHalf = true) {
  const half = Math.ceil(array.length / 2);
  if (firstHalf) {
    return array.splice(-half);
  } else {
    return array.splice(0, half);
  }
}

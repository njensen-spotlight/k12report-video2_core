const configFromWebpack = typeof __CONFIG__ !== 'undefined' ? { ...__CONFIG__ } : {};
const configFromPuppeteer = typeof __CONFIG__PUPPETEER__ !== 'undefined' ? { ...__CONFIG__PUPPETEER__ } : {};

const config = { ...configFromWebpack, ...configFromPuppeteer };

module.exports = config;

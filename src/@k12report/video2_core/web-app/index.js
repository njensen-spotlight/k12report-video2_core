const { createElement: h } = require('react');
const { render } = require('react-dom');
const { app } = require('./components');
const mapStackTrace = require('./mapStackTrace');

const div = (document.getElementById('app') || (() => render(h('div', { id: 'app' }), document.body))());

const renderApp = () => render(h(app), div);

try {
  mapStackTrace(renderApp);
} catch (error) {
  console.error(error);
} finally {
  div.classList.remove('loading');
}

/* For `npm run dev` to auto-reload upon code changes */
if (module.hot) { module.hot.accept(); }

const assert = require('assert');
const sinon = require('sinon');
const t = require('../../../../tests/utils');

describe(__dirname, () => {
  describe('default', () => {
    it('should work', () => {
      const selector = {};
      const keyframes = {};
      const options = {};
      const opts = { selector, keyframes, options };
      const spy = {
        querySelectorAll: sinon.spy((input) => {
          assert.equal(input, selector);
          return [{
            animate: sinon.spy((input1, input2) => {
              assert.deepEqual(input1, keyframes);
              assert.deepEqual(input2, {
                delay: 0,
                fill: 'forwards',
              });
              const animation = { pause: sinon.spy() };
              Object.defineProperty(animation, 'element', { set: sinon.spy() });
              Object.defineProperty(animation, 'options', { set: sinon.spy() });
              return animation;
            })
          }];
        }),
      };
      t.mockGlobals({
        document: { querySelectorAll: spy.querySelectorAll }
      });
      const preset = require('.').default;
      const output = preset(null, opts);
      // assert.deepEqual(output, [expected]);
      assert(spy.querySelectorAll.called);
      assert(spy.querySelectorAll.calledWith(selector));
      const [element] = spy.querySelectorAll.returnValues[0];
      assert(element.animate.called);
      assert(element.animate.calledWith(keyframes));
      const animation = element.animate.returnValues[0];
      assert(animation);
      assert(animation.pause);
      assert(animation.pause.called);
      const animationElement = Object.getOwnPropertyDescriptor(animation, 'element');
      assert(animationElement.set.calledWith(element));
      const animationOptions = Object.getOwnPropertyDescriptor(animation, 'options');
      assert(animationOptions.set.calledWith(options));
    });
  });

  describe('rest, basic', () => {
    let expected;
    const document = {
      querySelectorAll: () => [{
        animate: () => (expected = {
          pause: () => {},
        })
      }]
    };
    const opts = { selector: {}, keyframes: {}, options: {} };
    'hide, fadeIn, fadeOut, fadeInFrom, fadeInFromTop, fadeInFromLeft, zoomIn, zoomOut, bounce, zoomOutBackIn'
      .split(/, */).forEach(preset => it(preset, () => {
        t.mockGlobals({ document });
        assert.deepEqual(require('.')[preset](null, opts), [expected]);
      }));
  });

});

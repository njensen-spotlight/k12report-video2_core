const merge = require('merge-options');
const eases = require('web-animation-eases');

const presets = exports;

presets.default = (component, opts) => {
  const { selector, keyframes, ...restOpts } = opts;
  const options = restOpts.options || {};
  options.delay = options.delay || restOpts.delay || 0;
  // console.log(`selector:`, selector);
  let selected;
  try {
    // selected = Array.from(component.base.querySelectorAll(selector));
    // console.log('document.querySelectorAll(selector):', document.querySelectorAll(selector));
    selected = Array.from(document.querySelectorAll(selector));
    // console.log('selected:', selected);
  } catch (error) {
    throw new Error(`Selector '${selector}' didn't match any elements. ${error.message}`);
  }
  // console.log(selector, selected);
  if (!selected || !selected.length) {
    throw new Error(`Selector '${selector}' didn't match any elements`);
  }
  // console.log(`selected:`, selected);
  return Array.from(selected).map((element) => {
    const animationOptions = merge({
      fill: 'forwards'
    }, options);
    // console.log(`keyframes:`, keyframes);
    let animation;
    try {
      animation = element.animate(keyframes, animationOptions);
    } catch (error) {
      throw new Error(`Selector '${selector}' couldn't be animated. ${error.message}`);
    }
    animation.element = element;
    // console.log(`keyframes, options:`, keyframes, options);
    animation.options = JSON.parse(JSON.stringify(options));
    // log
    animation.pause();
    return animation;
  });
};

presets.hide = (component, opts) => presets.default(component, merge({
  component,
  keyframes: { opacity: [0, 0] }
}, opts));

presets.fadeIn = (component, opts) => presets.default(component, merge({
  component,
  keyframes: { opacity: [0, 1] },
  options: { duration: 500 }
}, opts));

presets.fadeOut = (component, opts) => presets.default(component, merge({
  component,
  keyframes: { opacity: [1, 0] },
  options: { duration: 500 }
}, opts));

presets.fadeInFrom = (component, opts) => presets.fadeIn(component, merge({
  keyframes: {
    transform: [`translate${opts.direction || 'X'}(${(opts.magnitude || 30) * (opts.opposite ? -1 : 1)}px)`, `translate${opts.direction || 'X'}(0)`]
  }
}, opts));

presets.fadeInFromTop = (component, opts) => presets.fadeInFrom(component, merge({
  direction: 'y',
  magnitude: -30
}, opts));

presets.fadeInFromLeft = (component, opts) => presets.fadeInFrom(component, merge({
  direction: 'x',
  magnitude: -30
}, opts));

presets.zoomIn = (component, opts) => presets.default(component, merge({
  component,
  keyframes: eases.elasticInOut({ keyframes: 100 })((x) => ({
    transform: `scale(${x})`
  })),
  options: { duration: 500 }
}, opts));

presets.zoomOut = (component, opts) => presets.default(component, merge({
  component,
  keyframes: eases.bounceOut({ keyframes: 100 })((x) => ({
    opacity: x,
    transform: `scale(${-3 * x + 4})`
  })),
  options: { duration: 500 }
}, opts));

presets.bounce = (component, opts) => presets.default(component, merge({
  component,
  keyframes: {
    transform: [
      'translateY(0)',
      'translateY(10px)',
      'translateY(-10px)',
      'translateY(0)',
    ]
  },
  options: { duration: 500, iterations: Infinity, loop: true }
}, opts));

presets.zoomOutBackIn = (component, opts) => presets.default(component, merge({
  component,
  keyframes: [{
    opacity: 1,
    transform: 'scale(1)'
  }, {
    easing: 'ease-in',
    transform: 'scale(1.5)',
    offset: 0.1
  }, {
    easing: 'ease-out',
    transform: 'scale(1.5)',
    offset: 0.9
  }, {
    opacity: 1,
    transform: 'scale(1)'
  }],
  options: { duration: 1000 }
}, opts));

try {
  require(__WEB_APP_DIR__ + '/presets');
  // Object.assign(presets, require(__WEB_APP_DIR__ + '/presets'));
} catch (error) {
  if (error.code !== 'MODULE_NOT_FOUND') throw error;
  // console.log('error.code:', error.code);
  // console.log('error:', error);
}

const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const sinon = require('sinon');

describe(__dirname, () => {
  describe('getAllAnimationsFromASlide', () => {
    describe('()', () => it('returns empty array', () => {
      const _ = require('.');
      assert.deepEqual(
        _.getAllAnimationsFromASlide(),
        []
      );
    }));
    describe('([])', () => it('returns empty array', async () => {
      const _ = require('.');
      assert.deepEqual(
        await _.getAllAnimationsFromASlide([]),
        []
      );
    }));
    describe('([{preset: unknown}])', () => it('throws', async () => {
      const _ = require('.');
      assert.throws(
        () => _.getAllAnimationsFromASlide([{ preset: 'unknown' }])
      );
    }));
    describe('([{}]), preset => []', () => it('returns empty array', async () => {
      const _ = proxyquire('.', { '../presets': { default: () => [] } });
      assert.deepEqual(
        await _.getAllAnimationsFromASlide([{}]),
        []
      );
    }));
    describe('([{}]), preset => [{}]', () => it('returns empty array', async () => {
      const _ = proxyquire('.', { '../presets': { default: () => [{}] } });
      assert.deepEqual(
        await _.getAllAnimationsFromASlide([{}]),
        [{
          options: {
            delay: 0,
            duration: 0,
            totalDuration: 0,
          }
        }]
      );
    }));
  });


  describe('getPlayFinishFromMediaElement', () => {
    describe('play', () => it('should work', async () => {
      const spy = {
        play: sinon.spy(),
        addEventListener: sinon.spy(),
        removeEventListener: sinon.spy(),
      };
      const _ = require('.');
      const { play, finished } = _.getPlayFinishFromMediaElement({
        play: spy.play,
        ended: true,
        addEventListener: spy.addEventListener,
        removeEventListener: spy.removeEventListener,
      });
      play();
      assert(spy.play.called);
      assert(spy.addEventListener.calledWith('timeupdate'));
      const [, handler] = spy.addEventListener.getCalls()[0].args;
      handler();
      assert(spy.removeEventListener.calledWith('timeupdate', handler));
      await finished;
    }));
    describe('play {repeat: true}', () => it('should work', async () => {
      const spy = {
        play: sinon.spy(),
        addEventListener: sinon.spy(),
        removeEventListener: sinon.spy(),
      };
      const _ = require('.');
      const { play, finished } = _.getPlayFinishFromMediaElement({
        play: spy.play,
        // ended: true,
        addEventListener: spy.addEventListener,
        removeEventListener: spy.removeEventListener,
      }, { repeat: true });
      play();
      assert(spy.play.called);
      assert(spy.addEventListener.calledWith('timeupdate'));
      const [, handler] = spy.addEventListener.getCalls()[0].args;
      handler();
      assert(spy.addEventListener.calledWith('timeupdate'));
      assert(spy.removeEventListener.notCalled);
    }));
    describe('play {progress}', () => it('should work', async () => {
      const spy = {
        play: sinon.spy(),
        addEventListener: sinon.spy(),
        progress: sinon.spy(),
      };
      const _ = require('.');
      const { play } = _.getPlayFinishFromMediaElement({
        play: spy.play,
        addEventListener: spy.addEventListener,
      }, {
        progress: { update: spy.progress }
      });
      play();
      assert(spy.play.called);
      assert(spy.addEventListener.calledWith('timeupdate'));
      const [, handler] = spy.addEventListener.getCalls()[0].args;
      const media = spy.addEventListener.thisValues[0];
      media.currentTime = 'currentTime';
      handler();
      assert(spy.progress.called);
      assert.deepEqual(spy.progress.getCalls()[0].args[0], { duration: media.currentTime });
    }));
    describe('pause', () => it('should work', async () => {
      const spy = {
        pause: sinon.spy(),
        removeEventListener: sinon.spy(),
      };
      const _ = require('.');
      const { pause } = _.getPlayFinishFromMediaElement({
        pause: spy.pause,
        removeEventListener: spy.removeEventListener,
      }, {});
      pause();
      assert(spy.pause.called);
      assert(spy.removeEventListener.called);
    }));
    describe('pause {repeat}', () => it('should work', () => {
      const spy = {
        pause: sinon.spy(),
        removeEventListener: sinon.spy(),
      };
      const _ = require('.');
      const { pause } = _.getPlayFinishFromMediaElement({
        pause: spy.pause,
        removeEventListener: spy.removeEventListener,
      }, { repeat: true });
      pause();
      assert(spy.pause.notCalled);
      assert(spy.removeEventListener.notCalled);
    }));
    describe('pause {repeat, force}', () => it('should work', async () => {
      const spy = {
        pause: sinon.spy(),
        removeEventListener: sinon.spy(),
      };
      const _ = require('.');
      const { pause } = _.getPlayFinishFromMediaElement({
        pause: spy.pause,
        removeEventListener: spy.removeEventListener,
      }, { repeat: true });
      pause({ force: true });
      assert(spy.pause.called);
      assert(spy.removeEventListener.called);
    }));
  });

  describe('getAllAudiosFromASlide', () => {

    describe('()', () => it('should resolve to empty array', async () => {
      const _ = require('.');
      assert.deepEqual(await _.getAllAudiosFromASlide(), []);
    }));

    describe('([])', () => it('should resolve to empty array', async () => {
      const _ = require('.');
      assert.deepEqual(await _.getAllAudiosFromASlide([]), []);
    }));

    describe('([{}])', () => it('should resolve to empty array', async () => {
      const _ = require('.');
      assert.deepEqual(await _.getAllAudiosFromASlide([{}]), []);
    }));

    describe('([{src}])', () => it('should resolve to empty array', async () => {
      const spy = {
        getAudioData: sinon.spy(i => i),
        getAudioElement: sinon.spy(i => i),
        connect: sinon.spy(i => i),
      };
      const audioSlide = { audio: 'audio' };
      const _ = proxyquire('.', {
        '../../../modules/audio': () => ({
          getAudioData: spy.getAudioData,
          getAudioElement: spy.getAudioElement,
          connect: spy.connect,
        })
      });
      // _.getAllAudiosFromASlide([audio]);
      const [audio] = await _.getAllAudiosFromASlide([audioSlide]);
      assert(spy.getAudioData.calledWith(audioSlide));
      assert(spy.getAudioElement.calledWith(audioSlide));
      assert(spy.connect.calledWith(audioSlide));
      assert.deepEqual(audio.audioData, audioSlide);
      assert.deepEqual(audio.mediaElement, audioSlide);
    }));
  });

  describe('getAllVideosFromASlide', () => {

    describe('()', () => it('should return empty', async () => {
      const _ = require('.');
      assert.deepEqual(await _.getAllVideosFromASlide(), []);
    }));

    describe('([])', () => it('should return empty', async () => {
      const _ = require('.');
      assert.deepEqual(await _.getAllVideosFromASlide([]), []);
    }));

    describe('([{src}])', () => it('should return empty', async () => {
      const videoData = { src: 'src' };
      const _ = require('.');
      const [video] = await _.getAllVideosFromASlide([videoData]);
      assert.equal(typeof video.play, 'function');
      assert.equal(typeof video.pause, 'function');
    }));

  });

  describe('setCaptionsFromMediaArray', () => {
    describe('([captions])', () => it('should return empty', async () => {
      const _ = require('.');
      await _.setCaptionsFromMediaArray();
      await _.setCaptionsFromMediaArray([]);
      await _.setCaptionsFromMediaArray(['captions']);
    }));
  });

  describe('wordWrap', () => {
    describe('(ab cde, 3)', () => it('[ab, cde]', async () => {
      const _ = require('.');
      assert.deepEqual(_.wordWrap('ab cde', 3), ['ab', 'cde']);
    }));
    describe('(abc d e fg i, 3)', () => it('[abc, d e, fg, i]', async () => {
      const _ = require('.');
      assert.deepEqual(_.wordWrap('abc d e fg i', 3), ['abc', 'd e', 'fg', 'i']);
    }));
  });
});

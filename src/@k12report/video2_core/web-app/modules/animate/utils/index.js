const { flatten } = require('lodash');
const defer = require('p-defer');
const wordWrap = require('word-wrap');
const presets = require('../presets');
const { state } = require('../../../store');
const commonUtils = require('../../../utils');
const data = require('../../../data');
const config = require('../../../config');

const frameRate = config.frameRate || data.metadata.frameRate;
const timePeriod = 1000 / frameRate;

const _ = exports;

_.getAllAnimationsFromASlide = (animations, { progress } = {}) => Promise.all(flatten((animations || []).map(animation => {
  const { preset, ...restOpts } = animation;
  if (preset && !presets[preset]) {
    throw new Error(`Invalid animation preset: '${preset}'`);
  }
  const animator = presets[preset] || presets.default;
  const animations = animator(this, animation);
  return animations.map(animation => {
    const options = animation.options = animation.options || { delay: 0, duration: 0 };
    Object.assign(options, restOpts);
    const delay = options.delay = options.delay || 0;
    const duration = options.duration = options.duration || 0;
    const totalDuration = options.totalDuration = options.totalDuration || (delay + duration);
    // max(totalDuration);
    return animation;
  });
})));

_.getPlayFinishFromMediaElement = (mediaElement, { repeat = false, delay = 0, progress } = {}) => {
  const { promise: finished, resolve, reject } = defer();
  const checkEnded = () => {
    progress && progress.update({ duration: mediaElement.currentTime });
    if (mediaElement.ended) {
      if (repeat) {
        mediaElement.play();
      } else {
        resolve();
        mediaElement.removeEventListener('timeupdate', checkEnded);
      }
    }
  };
  const play = async () => {
    if (delay) await commonUtils.delay(delay);
    mediaElement.play();
    mediaElement.addEventListener('timeupdate', checkEnded);
    return finished;
  };
  const pause = ({ force = false } = {}) => {
    if (!repeat || force) {
      mediaElement.pause();
      resolve();
      mediaElement.removeEventListener('timeupdate', checkEnded);
    }
  };
  return { play, pause, finished, repeat, delay };
};

_.getAllAudiosFromASlide = (audios, props) => Promise.all(flatten((audios || []).filter(a => a && a.audio).map(async audio => {
  const { connect, getAudioData, getAudioElement, ...audioRest } = require('../../../modules/audio')();
  const audioData = getAudioData(audio);
  const mediaElement = await getAudioElement(audioData);
  connect(mediaElement);
  const { play, pause, finished, ...rest } = _.getPlayFinishFromMediaElement(mediaElement, { repeat: audio.repeat, delay: audio.delay, ...props });
  return { play, pause, finished, mediaElement, audioData, caption: audioData.caption, ...audioRest, ...rest };
})));

_.getAllVideosFromASlide = (videos, props) => Promise.all(flatten((videos || []).map(async video => {
  const { connect } = require('../../../modules/audio')();
  const videoData = data.video[video.video || video];
  const videoSrc = require(__WEB_APP_DIR__ + '/videos/' + videoData.src);
  const mediaElement = props.sceneComponent.sceneComponent.current.querySelector(videoData.selector || 'video');
  mediaElement.setAttribute('src', videoSrc);
  connect(mediaElement);
  const { play, pause, finished, ...rest } = _.getPlayFinishFromMediaElement(mediaElement, { repeat: video.repeat, delay: video.delay, ...props });
  return { play, pause, finished, mediaElement, ...rest, ...videoData };
})));

_.setCaptionsFromMediaArray = array => {
  const captions = (array || []).filter(Boolean).map(m => {
    const caption = m.caption || m['audio-as-text'];
    if (typeof caption === 'string') return caption;
    else if (Array.isArray(caption) && caption[0]['caption-segment']) {
      return caption.map(c => c['caption-segment']).join(' ');
    } else {

      // console.warn('One of the slides had an invalid caption:', caption, m);
      // console.error(m);
      // console.error(caption);
      // throw new Error('Invalid captions (see console logs for details)');
      /* Or maybe not throw/warn here, because `array` could just be an animation slide */
    }
  }).filter(Boolean);
  // console.log(`captions:`, captions);
  // console.log({ array, captions });
  state.captions = captions.join('\n');
};

_.maxDuration = (array) => {
  let maxDuration = 0;
  const add = c => maxDuration = Math.max(maxDuration, _.maxDurationFramerateAdjusted(c));
  for (const media of array) {
    if (media.options) {
      add(media.options.totalDuration || media.options.duration);
    } else if (media.audioData) {
      add((media.audioData.duration || 0) + (media.audioData.delay || media.delay || 0) || 0);
      // console.error({ media: { audioData: media.audioData } });
    } else if (media.duration) {
      add(media.duration);
    }
  }

  return maxDuration;
};

_.maxDurationFramerateAdjusted = (duration) => {
  const remainder = duration % timePeriod;
  return remainder ? duration - (duration % timePeriod) + timePeriod : duration;
};

_.wordWrap = (string, width = 80) => wordWrap(string, {
  width,
  indent: '',
  trim: true,
  newline: '__WRAP__',
}).split(/__WRAP__/g).map(s => s.trim()).filter(Boolean);

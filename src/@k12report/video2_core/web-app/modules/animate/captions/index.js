const delay = require('delay');
const { metadata } = require('../../../data');
const { state } = require('../../../store');
const { deDupe } = require('../../../utils');
const _ = require('../utils');

const captionsWordWrap = metadata.captionsWordWrap || 80;
const captions = new Map;

const updateState = () => state.captions = deDupe(Array.from(captions.values()).map(inner => (Array.isArray(inner) ? inner : [inner]).filter(Boolean)).filter(Boolean));

module.exports = class Captions {

  constructor(captions, { delay = 0, onUpdate = () => {}, duration } = {}) {
    this.onUpdate = onUpdate;
    this.load(captions);
    this.delay = delay || (captions.delay) || 0;
    this.duration = duration || (captions.duration) || (captions.audioData && captions.audioData.duration) || 0;
    // console.log('onUpdate:', onUpdate);
    // console.log('this.onUpdate:', this.onUpdate);
  }

  load(captions) {
    const log = console.log.bind(console, { captions });
    if (!captions) throw new Error('Need captions');
    if (captions.captions) captions = captions.captions;
    if (captions.caption) captions = captions.caption;
    if (captions['audio-as-text']) captions = _.wordWrap(captions['audio-as-text'], captionsWordWrap);
    if (typeof captions === 'string') {
      // this.captions = [{ 'caption-segment': captions }];
      this.captions = _.wordWrap(captions, captionsWordWrap).map(c => ({ 'caption-segment': c }));
    } else if (Array.isArray(captions)) {
      this.captions = captions.map(caption => {
        if (typeof caption === 'string') {
          return { 'caption-segment': caption };
        } else if (caption && caption['caption-segment']) {
          return caption;
        }
      }).filter(Boolean);
    } else {
      // log('Invalid captions');
      // throw new Error('Invalid captions');
      this.captions = [];
    }
    this.update();
  }

  get current() {
    let existing = captions.get(this);
    if (!existing) {
      existing = [];
      captions.set(this, existing);
    }
    return existing;
  }
  update(caption) {
    let { current } = this;
    if (current.includes(caption)) return;
    captions.set(this, current = [...current, caption].filter(Boolean));
    updateState();
    if (typeof this.onUpdate === 'function')
      this.onUpdate(current);
    else console.log({ 'typeof this.onUpdate': typeof this.onUpdate, 'this.onUpdate': this.onUpdate, this: this });
  }

  async play() {
    for (const caption of this.captions) {
      await delay(this.getDelay(caption, caption === this.captions[0]));
      this.update(caption['caption-segment']);
    }
  }

  next(time = 0) {
    time = time || 0;
    // time = parseInt(time, 10);
    for (let i = 0; i < this.captions.length; i++) {
      const caption = this.captions[i];
      // const cap = caption['caption-segment'];
      let delay = 0;
      for (let j = 0; j <= i + 1; j++) {
        const caption = this.captions[j];
        if (caption) {
          delay += this.getDelay(caption, caption === this.captions[0]);
        } else {
          delay += Infinity;
        }
      }
      // console.log({ time, delay, cap, i });
      if (time < delay) {
        this.update(caption['caption-segment']);
        return;
      }
    }
  }

  getDelay(caption, first) {
    let delay = 0;
    if (this.delay) {
      delay += this.delay;
    }
    if (caption.delay) {
      delay += caption.delay;
    }
    if (!delay && !first) {
      delay += this.duration / this.captions.length;
    }
    return delay;
  }

  delete() {
    if (captions.has(this)) {
      captions.delete(this);
      updateState();
    }
  }

};

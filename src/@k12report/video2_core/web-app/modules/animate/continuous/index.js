const _ = require('../utils');
const Captions = require('../captions');
const audioMetaData = require('../../audio/metadata');
const delay = require('delay');

module.exports = async function continuous(props) {
  return Promise.all([
    animations.call(this, props),
    audios.call(this, props),
    videos.call(this, props),
  ])
}

async function animations(props) {
  return common.call(this, { context: 'animations', extractor: _.getAllAnimationsFromASlide, ...props });
}

async function audios(props) {
  return common.call(this, { context: 'audios', extractor: _.getAllAudiosFromASlide, ...props });
}

async function videos(props) {
  return common.call(this, { context: 'videos', extractor: _.getAllVideosFromASlide, ...props });
}

async function common(props) {
  const start = new Date;
  const slideContext = props.slide[props.context];
  const array = await props.extractor.call(this, slideContext, { ...props, sceneComponent: this });
  const pauseAll = [];
  const captions = array.map(media => new Captions(media, {
    onUpdate: (captions) => {
      audioMetaData.update({ ...media, ...props, captions });
    }
  }));

  const maxDuration = _.maxDuration(array);
  const promise = Promise.all(array.map(async (media, i) => {
    media.play();
    captions[i].play();
    pauseAll.push((opts) => media.pause(opts));
    if (slideContext[i] && slideContext[i].duration) {
      await delay(slideContext[i].duration);
      media.pause();
      captions[i].delete();
    } else {
      await media.finished;
      captions[i].delete();
    }
  }));
  await delay(maxDuration);
}

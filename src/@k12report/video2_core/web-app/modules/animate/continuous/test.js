const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const sinon = require('sinon');

describe(__dirname, () => {

  it('should return a promise', async () => {
    const method = require('.');
    assert.deepEqual(await method({ slide: {} }), [
      undefined,
      undefined,
      undefined
    ]);
  });

  it('default', async () => {
    const spy = {
      delay: sinon.spy(),
      play: sinon.spy(),
      pause: sinon.spy(),
    };
    const method = proxyquire('.', {
      // delay: () => count(new Error('delay called')),
      delay: spy.delay,
      '../utils': {
        getAllAnimationsFromASlide: i => i,
        getAllAudiosFromASlide: i => i,
        getAllVideosFromASlide: i => i,
        setCaptionsFromMediaArray: i => i,
        maxDuration: i => i,
      }
    });
    const common = {
      play: spy.play,
      pause: spy.pause,
    };

    await await method({ slide: { animations: [common], audios: [common], videos: [common] } });
    // assert(spy.delay.notCalled);
    assert(spy.play.called);
    assert(spy.pause.notCalled);
  });

  it('slide.duration', async () => {
    const spy = {
      delay: sinon.spy(),
      play: sinon.spy(),
      pause: sinon.spy(),
    };
    const duration = 1;
    const method = proxyquire('.', {
      delay: spy.delay,
      '../utils': {
        getAllAnimationsFromASlide: i => i,
        getAllAudiosFromASlide: i => i,
        getAllVideosFromASlide: i => i,
        setCaptionsFromMediaArray: i => i,
        maxDuration: i => i,
      }
    });
    const common = {
      play: spy.play,
      pause: spy.pause,
    };
    await await method({ slide: { duration, animations: [common], audios: [common], videos: [common] } });
    // assert(spy.delay.calledWith(duration));
    assert(spy.play.called);
    // assert(spy.pause.called);
  });

  it('slide[context].duration', async () => {
    const spy = {
      delay: sinon.spy(),
      play: sinon.spy(),
      pause: sinon.spy(),
    };
    const duration = 1;
    const method = proxyquire('.', {
      delay: spy.delay,
      '../utils': {
        getAllAnimationsFromASlide: i => i,
        getAllAudiosFromASlide: i => i,
        getAllVideosFromASlide: i => i,
        setCaptionsFromMediaArray: i => i,
        maxDuration: i => i,
      }
    });
    const common = {
      duration,
      play: spy.play,
      pause: spy.pause,
      // get finished() {
      //   count(new Error('.finished promise accessed'));
      //   return Promise.resolve();
      // }
    };
    await await method({ slide: { animations: [common], audios: [common], videos: [common] } });
    assert(spy.delay.calledWith(duration));
    assert(spy.play.called);
    assert(spy.pause.called);
  });
});

const _ = require('../utils');
const Captions = require('../captions');
const { arrify, once, delay } = require('../../../utils');
const { debug } = require('../../../store');
const logger = require('../../../logger');

module.exports = async function*({ scene, slide, slideIndex, period, progress }) {
  const iterators = new Set([
    animations.call(this, { slide, period }),
    audios.call(this, { slide, period }),
    videos.call(this, { slide, period }),
  ]);

  let duration = 0;
  let lastInput = {};

  const next = async (iterator) => {
    const { done, value } = await iterator.next(lastInput);
    if (value && value.context) {
      const { context } = value;
      debug[scene] = { ...(debug[scene] || {}), slideIndex, [context]: { done, value } };
    }
    if (done || (value && value.done) || (value && value.length ? value : []).find(v => v && v.done)) {
      iterators.delete(iterator);
    }
    duration = Math.max(duration, ...arrify(value).map(v => v.currentTime));
    return value;
  };

  while (iterators.size) {
    lastInput = yield await Promise.all([...iterators].map(next));
    progress.update({ duration });
  }

  slide.duration = duration;

  return { duration };
};

async function* animations({ slide, period, periodMultiplier = 1 }) {
  const set = new Set(await _.getAllAnimationsFromASlide(slide.animations));
  let value = {};
  let currentTime = 0;
  while (set.size) {
    value = value || {};
    period = value.period || period;
    await Promise.all(Array.from(set).map(animation => {
      animation.currentTime += period * (value.periodMultiplier || periodMultiplier || 1);
      currentTime = Math.max(currentTime, animation.currentTime);
      if (animation.currentTime >= animation.options.totalDuration) {
        if (animation.options && (animation.options.loop || animation.options.repeat)) {
          currentTime = animation.currentTime = 0;
        } else {
          set.delete(animation);
        }
      }
    }));
    value = yield { context: 'animations', currentTime, done: !set.size };
  }
  return { context: 'animations', currentTime };
}

async function* audios(props) {
  // yield* awaitMedia(await _.getAllAudiosFromASlide(slide.audios), { slide, period });
  yield* audioVideoCommon.call(this, { context: 'audios', extractor: _.getAllAudiosFromASlide, ...props });
}

async function* videos(props) {
  // yield* awaitMedia(await _.getAllVideosFromASlide(slide.videos), { slide, period });
  yield* audioVideoCommon.call(this, { context: 'videos', extractor: _.getAllVideosFromASlide, ...props });
}

async function* audioVideoCommon({ slide, period, context, extractor, periodMultiplier = 1 }) {
  const slideContext = slide[context];
  const array = await extractor.call(this, slideContext, { sceneComponent: this });
  const maxDuration = _.maxDuration(array) / 1000;
  const set = new Set(array);
  period /= 1000;
  let value = {};
  let currentTime = 0;
  const captions = array.map((m) => new Captions(m));
  while (set.size) {
    value = value || {};
    if (value.isParentFinished) break;
    period = value.period || period;
    await Promise.all(array.map(async (media, i) => {
      const expectedCurrentTime = toFixed((media.currentTime || media.mediaElement.currentTime) + period * (value.periodMultiplier || periodMultiplier || 1));
      const actualDuration = toFixed(((media.delay || 0) / 1000) + media.mediaElement.duration);
      const seeked = once(media.mediaElement, 'seeked');
      media.currentTime = media.mediaElement.currentTime = expectedCurrentTime;
      try {
        if (media && media.audioData) {
          const { currentTime, audioData: { name, duration } } = media
          logger.debug({
            audio: {
              [name]: { currentTime, duration }
            }
          });
        }
      } catch (error) {}
      await Promise.race([seeked, delay(1000)]);
      captions[i].next(toFixed((expectedCurrentTime * 1000) + (media.delay || 0)));
      if (expectedCurrentTime >= maxDuration) {
        if (media.repeat) {
          media.currentTime = media.mediaElement.currentTime = 0;
        } else {
          slideContext[i].duration = expectedCurrentTime * 1000;
          set.delete(media);
        }
      }
      currentTime = Math.max(currentTime, expectedCurrentTime * 1000);
    }));
    const done = !set.size;
    if (done) {
      captions.forEach(caption => caption.delete());
    }
    value = yield { context, currentTime, done };
  }
  return { context, currentTime };
}

function toFixed(num, digits = 2) {
  if (num && num.toFixed) {
    return parseFloat(num.toFixed(digits));
  }
}

const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const sinon = require('sinon');

describe(__dirname, () => {
  it('default', async () => {
    const spy = {
      delay: sinon.spy(),
      play: sinon.spy(),
      pause: sinon.spy(),
    };
    const method = proxyquire('.', {
      delay: spy.delay,
      '../utils': {
        getAllAnimationsFromASlide: i => i,
        getAllAudiosFromASlide: i => i,
        getAllVideosFromASlide: i => i,
        setCaptionsFromMediaArray: i => i,
      }
    });
    const common = {
      play: spy.play,
      pause: spy.pause,
      // get finished() {
      //   return Promise.resolve();
      // },
      currentTime: 0,
      mediaElement: { currentTime: 0, duration: 2 },
      options: { totalDuration: 2000 },
    };
    const iterator = method({
      period: 1000,
      slide: {
        animations: [{ ...common, currentTime: 0, mediaElement: { ...common.mediaElement } }],
        audios: [{ ...common, currentTime: 0, mediaElement: { ...common.mediaElement } }],
        videos: [{ ...common, currentTime: 0, mediaElement: { ...common.mediaElement } }]
      },
      progress: { update: () => {} },
    });
    assert.deepEqual(await iterator.next(), {
      value: [
        { context: 'animations', currentTime: 1000, done: false },
        { context: 'audios', currentTime: 1000, done: false },
        { context: 'videos', currentTime: 1000, done: false }
      ],
      done: false
    });
    assert.deepEqual(await iterator.next(), {
      value: [
        { context: 'animations', currentTime: 2000, done: true },
        { context: 'audios', currentTime: 2000, done: true },
        { context: 'videos', currentTime: 2000, done: true }
      ],
      done: false
    });
    assert.deepEqual(await iterator.next(), {
      value: { duration: 2000 },
      done: true
    });
    assert(spy.delay.notCalled);
    assert(spy.play.notCalled);
    assert(spy.pause.notCalled);
  });
});

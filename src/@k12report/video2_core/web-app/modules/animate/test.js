const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const t = require('../../../tests/utils');

describe(__dirname, () => {

  // it('should return an async generator', () => {
  //   const animate = proxyquire('.', {
  //     'root/data': {metadata: {}}
  //   });
  //   assert(typeof animate().next().then === 'function');
  // });
  it('basic, empty', async () => {
    const animate = proxyquire('.', {
      ...t.mockData(),
    });
    const iterator = animate.call({
      props: { scene: { scene: 'scene', slides: [] } }
    });
    const output = [];
    for await (const value of iterator) {
      output.push(value);
    }
    assert.deepEqual(output, []);
  });
  it('basic, continuous, []', async () => {
    t.mockGlobals({
      window: { PUPPETEER_waitForScreenshot: false }
    });
    const animate = proxyquire('.', {
      ...t.mockData(),
      './continuous': i => i,
      // './intermittent': t.identityGenerator,
    });
    const slide = {};
    const iterator = animate.call({
      props: { scene: { scene: 'scene', slides: [slide] } }
    });
    const output = [];
    for await (const value of iterator) {
      output.push(value);
    }
    assert.deepEqual(output, [{
      frameRate: 25,
      period: 40,
      progress: {
        progress: {
          scene: 'scene',
          slideIndex: 0
        }
      },
      scene: 'scene',
      sharedStats: {
        animations: {},
        audios: {},
        videos: {}
      },
      slide: {},
      slideIndex: 0
    }]);
  });
  it('basic, intermittent, []', async () => {
    t.mockGlobals({
      window: { PUPPETEER_waitForScreenshot: true }
    });
    const animate = proxyquire('.', {
      ...t.mockData(),
      // './continuous': i=>i,
      './intermittent': t.identityGenerator,
    });
    const slide = {};
    const iterator = animate.call({
      props: { scene: { scene: 'scene', slides: [slide] } }
    });
    const output = [];
    for await (const value of iterator) {
      output.push(value);
    }
    assert.deepEqual(output, []);
  });
});

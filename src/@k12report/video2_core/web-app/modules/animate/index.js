const data = require('../../data');
const config = require('../../config');
const continuous = require('./continuous');
const intermittent = require('./intermittent');
const { state } = require('../../store');
const { progressLog } = require('../../utils');

module.exports = async function*() {

  const frameRate = config.frameRate || data.metadata.frameRate || 25;
  const period = 1000 / frameRate;

  const sceneLabel = this.props.scene.scene;
  const sharedSceneProps = { scene: this.props.scene.scene, frameRate, period };

  const progress = new progressLog({ scene: this.props.scene.scene });
  sharedSceneProps.progress = progress;

  state.scene = sceneLabel;

  for (let slideIndex = 0; slideIndex < this.props.scene.slides.length; slideIndex++) {
    const slide = this.props.scene.slides[slideIndex];
    state.slide = slide;

    progress.update({ slideIndex });

    const sharedStats = { audios: {}, videos: {}, animations: {} };
    const sharedSlideProps = { ...sharedSceneProps, sharedStats, slide, slideIndex };

    if (window.PUPPETEER_waitForScreenshot || state.intermittent) {
      yield* intermittent.call(this, sharedSlideProps);
    } else {
      yield await continuous.call(this, sharedSlideProps);
    }
  }

  progress.finish();
};

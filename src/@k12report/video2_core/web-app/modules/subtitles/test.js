const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');

describe(__dirname, () => {

  it('should work', () => {
    const subtitle = proxyquire('.', {
      subtitle: {
        stringify: i => i,
        stringifyVtt: i => i,
      }
    });

    assert.deepEqual(subtitle(), {
      srt: '',
      vtt: ''
    });

    assert.deepEqual(subtitle([{
      currentTime: new Date,
      text: 'test',
      settings: {},
    }], new Date), {
      srt: [{ start: 0, end: 3000, text: 'test', settings: {} }],
      vtt: [{ start: 0, end: 3000, text: 'test', settings: {} }]
    });

    assert.deepEqual(subtitle([{
      currentTime: new Date,
      text: 'test',
      settings: {},
    }, {
      currentTime: new Date,
      text: 'test',
      settings: {},
    }], new Date), {
      srt: [{ start: 0, end: 0, text: 'test', settings: {} },
        { start: 0, end: 3000, text: 'test', settings: {} }
      ],
      vtt: [{ start: 0, end: 0, text: 'test', settings: {} },
        { start: 0, end: 3000, text: 'test', settings: {} }
      ]
    });

  });

});

const subtitle = require('subtitle');

module.exports = (input, startTime) => {

  if (!input) return { srt: '', vtt: '' };

  const compatible = input.map(({ currentTime, text, settings }, i) => ({
    start: Number(currentTime) - Number(startTime),
    end: Number((input[i + 1] && Number(input[i + 1].currentTime)) || (Number(currentTime) + 3000)) - Number(startTime),
    text,
    settings,
  }));

  const srt = subtitle.stringify(compatible);
  const vtt = subtitle.stringifyVtt(compatible);

  return { srt, vtt };
};

/**
 * Exports from this module are wrapped in a memoized function call,
 * i.e. the exports are only initialized once the function is called,
 * and then the results are reused on subsequent function calls.
 *
 * This is needed because exports from this module must only be
 * initialized after a certain point - after user interaction.
 *
 * Simply put, use the module like this:
 *    const {audioCtx, mediaRecorder} = require('./modules/audio')()
 *                                                                ^ call
 */

const createRecorder = require('./recorder');
const { addEventListeners } = require('../../utils');
const data = require('../../data');

let audio;

module.exports = ({ reuseAudio = true } = {}) => {
  if (audio && reuseAudio) return audio;

  // const { data } = this.context;
  const audioCtx = new AudioContext();
  const { mediaRecorder, audioDataPromise, mediaRecordStartTime, subtitlesPromise } = createRecorder({ audioCtx });

  function connect(source) {
    try {
      const mediaSource = audioCtx.createMediaElementSource(source);
      mediaSource.connect(audioCtx.destination);
      mediaSource.connect(mediaRecorder.destination);
    } catch (error) {
      if (error.message.includes('HTMLMediaElement already connected previously to a different MediaElementSourceNode')) return;
      throw error;
    }
  }

  audio = { audioCtx, mediaRecorder, audioDataPromise, mediaRecordStartTime, subtitlesPromise, connect, getAudioData, getAudioElement };
  return audio;
};

function getAudioData(audio) {
  const audioData = data.audio[data.metadata.language || 'default'][audio.audio];
  if (!audioData || !audioData.src) {
    console.error({ audio, audioData });
    throw new Error('Invalid audio data (need `src`) for ' + JSON.stringify(audio));
  }
  return audioData;
}

async function getAudioElement(audioData) {
  const audioSrc = require(__WEB_APP_DIR__ + '/audios/' + (data.metadata.language || 'default') + '/' + audioData.src);
  const audioElement = new Audio(audioSrc);
  audioElement.crossOrigin = 'anonymous';

  // await new Promise((resolve, reject) => addEventListeners(audioElement, {
  //   canplaythrough: resolve,
  //   // canplay: resolve,
  //   error: () => {
  //     console.error('Error loading audio', audio, audioData);
  //     reject(audioElement.error);
  //   },
  // }));

  return audioElement;
}

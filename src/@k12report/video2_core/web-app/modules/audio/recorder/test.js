const assert = require('assert');
const sinon = require('sinon');
const t = require('../../../../tests/utils');

describe(__dirname, () => {

  let audioCtx;
  let destination;
  let silence;
  beforeEach(() => {
    destination = {};
    silence = { frequency: {}, connect: () => {} };
    audioCtx = {
      createMediaStreamDestination: () => destination,
      createOscillator: () => silence,
    };
  });

  describe('mediaRecorder', () => {
    it('ondataavailable', () => {
      const spy = {
        readAsDataURL: sinon.spy(),
      };
      const data = { data: 'data' };
      t.mockGlobals({
        Blob: class {
          constructor(audioDataBuffer) {
            return audioDataBuffer;
          }
        },
        FileReader: class {
          constructor() {
            return {
              readAsDataURL: spy.readAsDataURL,
            };
          }
        }
      });
      const recorder = require('.');
      const { mediaRecorder } = recorder({ audioCtx });
      mediaRecorder.ondataavailable({ data });
      mediaRecorder.onstop();
      assert(spy.readAsDataURL.calledWith([data]));
    });

  });

});

const defer = require('p-defer');

module.exports = ({ audioCtx }) => {

  const audioDataPromise = defer();
  const subtitlesPromise = defer();

  /* This will record audio in browser */
  const destination = audioCtx.createMediaStreamDestination();
  const mediaRecorder = new MediaRecorder(destination.stream);
  mediaRecorder.destination = destination;

  /* Add a silence at the beginning (before audio from any scenes/slides are played), otherwise audio isn't captured until any real audio is played */
  /* TODO: This will be deprecated soon, find another way to create silence */
  const silence = audioCtx.createOscillator();
  silence.frequency.value = 1;
  // Deprecation notice; more info: https://github.com/Tonejs/Tone.js/issues/282

  const audioDataBuffer = [];
  mediaRecorder.ondataavailable = e => {
    /* Collect data */
    /* Fires whenever mediaRecorder.requestData() is called (../animate/animate.js) */
    audioDataBuffer.push(e.data);
    // console.log(`[audio-progress] Added ${e.data.size} bytes in ${audioDataBuffer.length} packets`);
  };
  mediaRecorder.onstop = () => {
    /* Combine, convert the data and resolve the promise */
    // console.log('Media recorded');
    const fileReader = new FileReader();
    fileReader.onload = e => audioDataPromise.resolve(e.target.result);
    fileReader.readAsDataURL(new Blob(audioDataBuffer, { 'type': 'audio/ogg' }));
  };

  const mediaRecordStartTime = new Date();
  mediaRecorder.start();
  silence.connect(destination);

  return {
    mediaRecorder,
    audioDataPromise: audioDataPromise.promise,
    mediaRecordStartTime,
    subtitlesPromise,
  };
};

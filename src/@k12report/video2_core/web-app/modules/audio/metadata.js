const audioMetadata = exports;

audioMetadata.data = [];

audioMetadata.update = ({ mediaRecordStartTime, scene, slideIndex: slide, captions } = {}) => {
  if (!mediaRecordStartTime) return;
  if (!captions || !captions.length) return;
  const time = new Date - mediaRecordStartTime;
  // console.log('audioMetadata:', time, captions, scene, slide);
  audioMetadata.data.push({ time, captions, scene, slide });
};

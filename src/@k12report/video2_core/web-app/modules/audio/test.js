const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const sinon = require('sinon');
const t = require('../../../tests/utils');

describe(__dirname, () => {

  describe('basic', () => {
    it('returns audio object', () => {
      const audio = require('.');
      const result = audio();
      assert(result.audioCtx);
      assert(result.getAudioData);
      assert(result.mediaRecorder);
    });
  });

  describe('connect', () => {
    it('returns audio object', () => {
      const spy = {
        recorder: sinon.spy(() => ({ mediaRecorder })),
        createMediaElementSource: sinon.spy(() => ({ connect: spy.connect })),
        connect: sinon.spy(),
      };
      const source = {};
      // let audioCtx;
      const mediaRecorder = { destination: {} };
      const audio = proxyquire('.', {
        './recorder': spy.recorder,
      });
      const audioCtx = {
        destination: {},
        createMediaElementSource: spy.createMediaElementSource,
      };
      t.mockGlobals({ AudioContext: class { constructor() { return audioCtx; } } });
      const { connect } = audio({ reuseAudio: false });
      connect(source);
      assert(spy.recorder.calledWith({ audioCtx }));
      assert(spy.createMediaElementSource.calledWith(source));
      assert(spy.connect.calledWith(audioCtx.destination));
      assert(spy.connect.calledWith(mediaRecorder.destination));
    });
  });

  describe('getAudioData', () => {
    it('throws without src', () => {
      const audioData = { src: null };
      const data = {
        audio: { language: { audio: audioData } },
        metadata: { language: 'language' }
      };
      const audio = proxyquire('.', { '__WEB_APP_DIR__/data': data });
      const { getAudioData } = audio({ reuseAudio: false });
      assert.throws(() => getAudioData({ audio: 'audio' }));
    });
    it('returns audio object', () => {
      const audioData = { src: 'src' };
      const data = {
        audio: { language: { audio: audioData } },
        metadata: { language: 'language' }
      };
      const audio = proxyquire('.', {
        ...t.mockData(data),
      });
      const { getAudioData } = audio({ reuseAudio: false });
      assert.equal(getAudioData({ audio: 'audio' }), audioData);
    });
  });

  describe('getAudioElement', () => {
    it('throws without src', async () => {
      const audio = require('.');
      const { getAudioElement } = audio({ reuseAudio: false });
      await assert.rejects(() => getAudioElement({ audio: 'audio' }));
    });
  });


});

let smSt;
try {
  smSt = require('sourcemapped-stacktrace');
} catch (error) {
  console.log('Could not load sourcemapped-stacktrace.', error);
  smSt = { mapStackTrace: (e, cb) => cb(e) };
}

module.exports = fn => {
  try {
    return fn();
  } catch (error) {
    // console.log('mapStackTrace');
    mapStackTrace(error);
    // debugger;
  }
};

function mapStackTrace(error) {
  if (!error) throw new Error('Unknown error');
  if (error.mapStackTrace) throw error;

  if (!error.stack) throw error;

  if (error.stack || error) {
    try {
      smSt.mapStackTrace(error.stack || error, (stack) => {
        error.stack = error.stack.replace(/\n.*/, '\n' + stack.join('\n'));
        error.mapStackTrace = true;
        throw error;
      });
    } catch (e) {
      console.log('Could not load sourcemapped-stacktrace.', e.message);
      throw error;
    }
  } else throw error;

}

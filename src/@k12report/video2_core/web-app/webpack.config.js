const Path = require('path');
const clean = require('clean-webpack-plugin');
const extract = require('mini-css-extract-plugin');
const html = require('html-webpack-plugin');
const { DefinePlugin } = require('webpack');

module.exports = async (env, argv) => {
  const opts = Object.assign({
    cwd: process.cwd(),
    isDev: /webpack-dev/.test(process.env.npm_lifecycle_script || process.argv.join()),
    srcPath: Path.join(__dirname),
    webm: false,
    verbose: false,
  }, env, argv);
  opts.port = opts.port || process.env.PORT || 5000;

  const {
    webAppDir,
    cwd,
    isDev,
    srcPath,
    buildDir,
    webm,
    verbose,
  } = opts;

  const config = {};

  config.context = srcPath;
  config.entry = './init';
  config.output = {
    filename: 'bundle.js',
    path: buildDir,
    sourceMapFilename: '[file].map',
    devtoolModuleFilenameTemplate: _ => `file:///${_.absoluteResourcePath.replace(/\\/g, '/')}`,
  };

  config.mode = isDev ? 'development' : 'production';
  // config.mode = 'development';
  config.devtool = isDev ? 'cheap-module-source-map' : 'source-map';

  if (!verbose) {
    // config.stats = 'errors-only';
    config.stats = { cached: false, cachedAssets: false, children: false, chunks: false, chunkGroups: false, chunkModules: false, chunkOrigins: false, colors: false, depth: false, entrypoints: false, env: false, errors: true, errorDetails: false, excludeAssets: name => !name.match(/\.(js|css)$/), hash: false, maxModules: 1, modules: false, moduleTrace: false, performance: false, publicPath: false, timings: true, version: false, warnings: false, };
  }

  config.optimization = {
    minimize: false,
  };

  config.node = { __dirname: true, __filename: true };

  config.resolve = {
    // symlinks: false,
  };

  const { rules } = config.module = { rules: [] };

  rules.push({
    test: /\.css$/,
    use: [isDev ? 'style-loader' : extract.loader, 'css-loader']
  });
  rules.push({
    test: /\.styl$/,
    use: [
      isDev ? 'style-loader' : extract.loader,
      {
        loader: 'css-loader',
        options: {
          modules: true,
          localIdentName: '[local]_[hash:base64:5]',
          camelCase: true,
          sourceMap: true
        }
      },
      'stylus-loader'
    ]
  });
  rules.push({
    test: webm
      ? /\.(png|gif|jpe?g|woff|woff2|eot|ttf|svg|pdf|docx)(\?v=[0-9]\.[0-9]\.[0-9])?$/
      : /\.(png|gif|jpe?g|woff|woff2|eot|ttf|svg|pdf|docx|mp3|mp4)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    use: {
      loader: 'file-loader',
      // loader: 'url-loader',
      options: {
        // name: isDev ? '[path][name].[ext]' : '[hash].[ext]',
        name: '[hash].[ext]',
        // name: '[path][name].[ext]',
        limit: 0
      }
    }
  });
  if (webm) {
    rules.push({ test: /\.(mp3|mp4)$/, use: { loader: require.resolve('./utils/webpack-webm'), options: { disable: isDev } }, });
  }

  const plugins = config.plugins = [];
  if (!isDev) {
    plugins.push(new clean({ verbose: false }));
  }
  plugins.push(new extract({ filename: 'app.css' }));
  plugins.push(new html({ template: 'index.html.js' }));

  const processEnv = Object.keys(process.env).reduce((env, key) => ({ ...env, [key]: JSON.stringify(process.env[key]) }), {});
  plugins.push(new DefinePlugin({
    'process.env': JSON.stringify(process.env),
    __CONFIG__: JSON.stringify(opts),
    ...processEnv,
    // __CWD__: JSON.stringify(cwd),
    __WEB_APP_DIR__: JSON.stringify(webAppDir),
  }));

  config.devServer = {
    port: opts.port,
    disableHostCheck: true,
    historyApiFallback: true,
  };

  return config;
};

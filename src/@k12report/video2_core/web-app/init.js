const mapStackTrace = require('./mapStackTrace');

if (!window.timeStart) {
  window.timeStart = window.timeEnd = window.timeRestart = () => {};
}

mapStackTrace(() => require('.'));

window.onerror = (message, source, lineno, colno, error) => {
  mapStackTrace(() => { throw error; });
};

window.addEventListener('error', function(e) {
  mapStackTrace(() => { throw e.error; });
  e.preventDefault();
  return false;
});

window.addEventListener('unhandledrejection', function(e) {
  mapStackTrace(() => { throw e.reason; });
  e.preventDefault();
  return false;
});

const _ = require('.');
const assert = require('assert');
const sinon = require('sinon');

describe(__dirname, () => {

  describe('_.required', () => it('should throw', () => {
    assert.throws(() => _.required());
    assert.throws(() => _.required('name'));
  }));

  describe('_.arrify', () => it('should arrify', () => {
    assert.deepEqual(_.arrify(), []);
    assert.deepEqual(_.arrify([]), []);
    assert.deepEqual(_.arrify([null]), []);
  }));

  describe('_.addEventListeners', () => {
    it('should addEventListeners', () => {
      const spy = {
        addEventListener: sinon.spy(),
        removeEventListener: sinon.spy(),
        handler: sinon.spy(),
      };
      _.addEventListeners({
        addEventListener: spy.addEventListener,
        removeEventListener: spy.removeEventListener,
      }, {
        'event': spy.handler
      });
      assert(spy.addEventListener.calledWith('event'));
      spy.addEventListener.getCalls()[0].args[1]();
      assert(spy.handler.called);
      assert(spy.removeEventListener.calledWith('event'));
    });
    it('handle rejection', () => {
      return assert.rejects(() => _.addEventListeners({
        addEventListener: (event, listener) => {
          listener();
        },
        removeEventListener: () => {}
      }, {
        event: () => { throw new Error('should\'ve been caught'); }
      }));
    });
  });

  describe('_.walkScenes', () => {
    it('should work', () => {
      assert.deepEqual(_.walkScenes(), []);
      const input = '[1,[1],[1,[1,[1]]]]';
      assert.deepEqual(_.walkScenes(JSON.parse(input), () => 2), JSON.parse(input.replace(/1/g, '2')));
    });
  });

  describe.skip('_.reConsole', () => {
    const backup = {};
    beforeEach(() => {
      backup.console = global.console;
    });
    afterEach(() => {
      global.console = backup.console;
    });
    it('should work', () => {
      const count = counter(1);
      global.console = {
        log: input => count(() => {
          assert.equal(input, 'prefix middle suffix');
        })
      };
      const console = _.reConsole('prefix', 'suffix');
      console.log('middle');
      return count.promise;
    });
  });

  describe('_.forAwaitOf', () => {
    it('should work', async () => {
      const spy = {
        next: sinon.spy(() => ({ value, done: true })),
        asyncFunction: sinon.spy(),
      };
      const value = {};
      await _.forAwaitOf({
        next: spy.next,
      }, spy.asyncFunction);
      assert(spy.next.called);
      assert(spy.asyncFunction.calledWith(value));
    });
  });

});

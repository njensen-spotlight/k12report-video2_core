const { mapStackTrace } = require('sourcemapped-stacktrace');

window.addEventListener('error', ({ error }) => handle(error));
window.addEventListener('unhandledrejection', ({ reason }) => handle(new Error(reason)));

async function handle(error) {
  const sourceMappedStackTrace = await new Promise(r => mapStackTrace(error.stack, r));
  error.stack = error.stack.replace(/\n.*/, '\n' + sourceMappedStackTrace.join('\n'));
  // throw error;
  if (window.reportErrorToPuppeteer) {
    window.reportErrorToPuppeteer({ message: error.message, stack: error.stack });
  } else {
    console.error(error);
  }
}

const { throttle } = require('lodash');

const set = new Set;

const log = throttle(set => {
  if (!set.size) return;
  const array = Array.from(set).map(item => `${item.scene}[${item.slideIndex}](${ms(item.duration)})`);
  console.info('Progress:', ...array);
}, 1000);

module.exports = class {
  constructor(initial = {}) {
    this.progress = initial;
    set.add(this.progress);
  }
  update(update) {
    Object.assign(this.progress, update);
    log(set);
    framesJsonProgress(set);
  }
  finish() {
    set.delete(this.progress);
  }
};

function ms(i) { return `${Math.round(parseInt(i || 0))}ms`; }

function framesJsonProgress(set) {
  if (typeof window === 'undefined' || !window.puppeteer) return;
  const progress = {};
  for (const { scene, slideIndex, duration } of set) {
    progress[scene] = {
      slideIndex,
      currentTime: duration,
    };
  }
  window.puppeteer.progress = progress;
}

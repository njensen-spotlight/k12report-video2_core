const defer = require('p-defer');

const _ = exports;

_.required = (name) => {
  throw new Error(`Missing required property${name ? `: ${name}` : ''}`);
};

_.arrify = i => (Array.isArray(i) ? i : [i]).filter(Boolean);

_.addEventListeners = (element, listeners) => {
  const deferred = defer();
  const removeAll = [];
  const resolve = (value) => {
    removeAll.forEach(r => r());
    deferred.resolve(value);
  };
  const reject = (error) => {
    removeAll.forEach(r => r());
    deferred.reject(error);
  };
  for (const event in listeners) {
    const listener = listeners[event];
    const wrapper = function(e) {
      try {
        const result = listener.call(this, e, { resolve, reject });
        if (listener.length <= 1) {
          resolve(result);
        } else {
          /* The listener chose to use `resolve/reject` itself */
        }
        return result;
      } catch (error) {
        reject(error);
        // throw error;
      }
    };
    element.addEventListener(event, wrapper);
    removeAll.push(() => element.removeEventListener(event, wrapper));
  }
  return deferred.promise;
};

_.walkScenes = (scenes, on) => (scenes || []).map(scene => Array.isArray(scene) ? _.walkScenes(scene, on) : on(scene));

_.reConsole = (prefix = '', suffix = '') => new Proxy(console, { get: (console, level) => (...msg) => console[level](prefix, ...msg, suffix) });

_.forAwaitOf = async (asyncIterator, asyncFunction) => {
  const last = { asyncIterator: {}, asyncFunction: {} };
  while (!last.asyncIterator.done) {
    last.asyncIterator = await asyncIterator.next(last.asyncFunction);
    last.asyncFunction = await asyncFunction(last.asyncIterator.value);
  }
};

_.delay = (timeout = 1000) => new Promise(resolve => setTimeout(resolve, timeout));

_.progressLog = require('./progress-log');

_.deDupe = array => {
  const ret = [];
  for (const item of array) {
    if (!ret.find(r => JSON.stringify(r) === JSON.stringify(item)))
      ret.push(item);
  }
  return ret;
};

_.once = (o, e) => {
  if (!o || !o.addEventListener) {
    console.warn(`Can't addEventListener(${e}) to`, o);
    return;
  }

  return new Promise(resolve => {
    const done = () => {
      o.removeEventListener(e, done);
      resolve();
    };
    o.addEventListener(e, done);
  });
};

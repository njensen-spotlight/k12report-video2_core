const { store, view } = require('react-easy-state');
const url = require('react-easy-url');
const config = require('./config');

exports.view = view;
exports.url = url;
exports.store = store;

exports.state = store({
  intermittent: 'intermittent' in url.params,
  debug: 'debug' in url.params || config.debug,
  controls: 'controls' in url.params,
  showFinishedMessage: 'fin' in url.params,
  captions: [[]],
});

exports.debug = store({});

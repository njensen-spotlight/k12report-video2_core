const h = require('hyperchain/react')({ style: require('./index.styl'), tagClass: true });
const { addEventListeners } = require('../../utils');

module.exports = ({ onClick }) => h.div.wrapper(h.button({
  id: 'start-button',
  onClick,
  ref: () => {
    // console.log('Pressing Start button is required to enable audio, see [Chrome Autoplay Policy](https://goo.gl/7K7WLu)');
    addEventListeners(document, { keypress: onClick });
  },
  title: 'Required to enable audio, see [Chrome Autoplay Policy](https://goo.gl/7K7WLu)',
}, 'Start (click me, or press any key to continue)'));

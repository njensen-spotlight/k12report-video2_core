const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const { shallow } = require('enzyme');
const sinon = require('sinon');
const h = require('hyperchain/react')();
const t = require('../../../tests/utils');

describe(__dirname, () => {
  it('output', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [{ component: sceneComponent }];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');
  });
  it('single scene', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [{ component: sceneComponent }];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    const sceneLoaderLastCallProps = sceneLoader.lastCall.args[0];
    assert(sceneLoaderLastCallProps.scene);
    assert(!sceneLoaderLastCallProps.scenes);
    assert.equal(sceneLoaderLastCallProps.scene, scenes[0]);
  });
  it('nested scenes', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [
      [{ component: sceneComponent }]
    ];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    const sceneLoaderLastCallProps = sceneLoader.lastCall.args[0];
    assert(!sceneLoaderLastCallProps.scene);
    assert(sceneLoaderLastCallProps.scenes);
    assert.deepEqual(sceneLoaderLastCallProps.scenes, scenes[0]);
  });
  it('onFinish', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [{ component: sceneComponent }];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    let sceneLoaderLastCall;
    sceneLoaderLastCall = sceneLoader.lastCall.args[0];
    sceneLoaderLastCall.onFinish();

    assert(props.onFinish.called);
    // const propsOnFinishLastCall = props.onFinish.lastCall.args[0];
    // assert.deepEqual(propsOnFinishLastCall, scenes);
  });
  it.skip('onFinish multiple scenes', async () => {
    const scenes = [{ component: sinon.spy(() => 'scene1') }, { component: sinon.spy(() => 'scene2') }];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    assert.deepEqual(wrapper.state(), { sceneIndex: 0 });

    assert(sceneLoader.args[0][0].onFinish);
    sceneLoader.args[0][0].onFinish();
    assert.deepEqual(wrapper.state(), { sceneIndex: 1 });
    // assert(sceneLoader.args[1][0].onFinish);

    // sceneLoaderProps.onFinish();
    // assert(props.onFinish.called);
    // const propsOnFinishLastCall = props.onFinish.lastCall.args[0];
    // assert.deepEqual(propsOnFinishLastCall, scenes);
  });
  it('onFinish nested scenes', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [
      [{ component: sceneComponent }]
    ];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    const sceneLoaderLastCall = sceneLoader.lastCall.args[0];
    sceneLoaderLastCall.onFinish(scenes[0]);

    const propsOnFinishLastCall = props.onFinish.lastCall.args[0];
    assert.deepEqual(propsOnFinishLastCall, scenes);
  });
});

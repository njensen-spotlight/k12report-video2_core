const { Component } = require('react');
const h = require('hyperchain/react')();

let counter = 0;

module.exports = class extends Component {
  constructor() {
    super();
    this.state = { sceneIndex: 0 };
  }
  componentWillMount() {
    // console.debug(`Loading ${this.props.scenes.length} scenes in sequence:`, this.props.scenes.map(s => s.scene || `[...${s.length} scenes in parallel]`).join(', '));
  }
  render() {
    const { sceneLoader } = require('..');
    const sceneIndex = this.state.sceneIndex;
    const scene = this.props.scenes[sceneIndex];

    if (!scene) {
      this.props.onFinish();
      return;
    }

    return h(sceneLoader, {
      ...this.props,
      key: counter++,
      scene: null,
      scenes: null,
      [Array.isArray(scene) ? 'scenes' : 'scene']: scene,
      onFinish: (scene) => {
        const nextSceneIndex = sceneIndex + 1;
        if (!this.props.scenes[nextSceneIndex]) {
          this.props.onFinish(this.props.scenes);
        } else {
          this.setState({ sceneIndex: nextSceneIndex });
        }
      },
      parallel: true,
    });
  }
};

const { Component } = require('react');
const methods = require('./methods');

class App extends Component {
  constructor() {
    super();
    this.state = {};
  }
}

Object.assign(App.prototype, methods);

module.exports = App;

exports.render = require('./render');
exports.animationProgress = require('./animationProgress');
exports.onFinish = require('./onFinish');
exports.componentDidCatch = require('./componentDidCatch');
exports.isFinished = function() { return this.finished; };
let ready;
exports.shouldComponentUpdate = (newProps, newState) => {
  if (!ready && newState.ready) {
    ready = true;
    return true;
  } else {
    return false;
  }
};

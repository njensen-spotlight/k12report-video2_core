const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const chain = require('chain-free');

describe(__dirname, () => {

  it('before ready', async () => {
    const method = proxyquire('.', {
      'hyperchain/react': () => chain(() => {}, {}, { base: () => 'h' }),
      '../../../../modules': {},
      '../../../../components': {},
      '../../../../scenes': {},
      '../../../../store': { state: {} },
    });

    const result = await method.call({
      subtitles: {},
      animationProgress: () => {},
      state: { ready: false }
    });

    assert.equal(result, 'h');
  });


  it('after ready', async () => {
    const method = proxyquire('.', {
      'hyperchain/react': () => chain(() => {}, {}, { base: () => 'h' }),
      '../../../../modules': {
        audio: () => {},
      },
      '../../../../components': {},
      '../../../../scenes': {},
      '../../../../store': { state: {} },
    });

    const result = await method.call({
      subtitles: {},
      animationProgress: () => {},
      onFinish: () => {},
      state: { ready: true }
    });
    assert.equal(result, 'h');

    // return count.promise;
  });

});

const h = require('hyperchain/react')({});
const { audio } = require('../../../../modules');
const scenes = require('../../../../scenes');
const cmp = require('../../../../components');
const { state } = require('../../../../store');

let counter = 0;

module.exports = function() {

  if (!this.state.ready) {
    /* Latest Chrome(/ium) requires a "user interaction" before it can play audio (this might change in future https://goo.gl/7K7WLu) */
    /* To achieve that, this will render a simple button which Puppeteer will click */
    return h(cmp.startButton, { onClick: () => this.setState({ ready: true }) });
    /* When the button has been clicked, `state.ready` is set and we move on */
  } else if (this.state.finished) {
    // return 'fin';
  } else {
    /* Initializes the audio module (more details there) */
    audio();

    const onFinish = this.onFinish.bind(this);
    const animationProgress = this.animationProgress.bind(this);

    return h._({
      key: counter++
    }, [
      h(cmp.sceneLoader, {
        key: counter++,
        parallel: true,
        scenes,
        onFinish,
        animationProgress,
      }),

      h(cmp.captions, { key: counter++ }),

      /* Overlays debug information when "debug" flag is set */
      state.debug && h(cmp.debug, this.state),

      state.controls && h(cmp.controls, this.state)
    ]);
  }
};

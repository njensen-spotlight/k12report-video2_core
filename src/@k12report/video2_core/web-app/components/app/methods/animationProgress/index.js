const { state } = require('../../../../store');

module.exports = async function(arg) {
  const {animateIterator, finished, isParentFinished} = arg;
  if (!(animateIterator || finished)) {
    return arg;
  }
  if (window.PUPPETEER_waitForScreenshot) {
    await window.PUPPETEER_waitForScreenshot(...arguments);
  } else if (state.intermittent) {
    await waitForInput();
  }
  return arg;
};

function waitForInput(){
  const keys = ['Enter'];
  const back = ['a'];
  const log = [keys => `Press [${keys.join('|')}] to progress animation`, key => `(you pressed: [${key}])`];
  console.log(log[0](keys));
  return new Promise((resolve, reject) => {
    document.addEventListener('keypress', (e) => {
      try {
        if (keys.includes(e.key)) {
          resolve({});
        } else if (back.includes(e.key)) {
          resolve({ periodMultiplier: -1 });
        } else {
          console.log(log[0](keys), log[1](e.key));
        }
      } catch (error) {
        reject(error);
      }
    }, { once: true });
  });
}

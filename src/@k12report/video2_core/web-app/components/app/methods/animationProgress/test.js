const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const sinon = require('sinon');
const t = require('../../../../../tests/utils');

describe(__dirname, () => {

  it('window.PUPPETEER_waitForScreenshot', async () => {
    const expected = { animateIterator: 'animateIterator' };
    t.mockGlobals({ window: { PUPPETEER_waitForScreenshot: sinon.spy() } });
    const method = require('.');
    await method(expected);
    assert(window.PUPPETEER_waitForScreenshot.calledWith(expected));
  });

  it('waitForInput', async () => {
    const expected = { animateIterator: 'animateIterator' };
    t.mockGlobals({
      window: { PUPPETEER_waitForScreenshot: false },
      document: {}
    });
    const spies = {
      keypressEnterResolve: sinon.spy(),
      keypressAResolve: sinon.spy(),
    };
    const method = proxyquire('.', {
      ...t.mockStore({ intermittent: true }),
      [require.resolve('../../../../utils')]: {
        addEventListeners: (document, { keypress }) => {
          keypress({ key: 'Enter' }, { resolve: spies.keypressEnterResolve });
          keypress({ key: 'a' }, { resolve: spies.keypressAResolve });
          keypress({ key: '' }, {});
        }
      }
    });
    await method(expected);
    assert(spies.keypressEnterResolve.called);
    assert(spies.keypressAResolve.called);
  });

});

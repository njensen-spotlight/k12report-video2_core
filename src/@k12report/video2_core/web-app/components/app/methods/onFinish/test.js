const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const assert = require('assert');
const t = require('../../../../../tests/utils');

describe(__dirname, () => {
  describe('basic', () => {
    it('should work', async () => {
      const spy = {
        finishedPromise: sinon.spy(),
        postMessage: sinon.spy(),
        stop: sinon.spy(),
        animationProgress: sinon.spy(),
      };
      t.mockGlobals({
        window: {
          puppeteer: { finishedPromise: { resolve: spy.finishedPromise }, },
          postMessage: spy.postMessage,
        }
      });
      const method = proxyquire('.', {
        '../../../../modules': {
          subtitles: s => s,
          audio: () => ({
            mediaRecorder: { stop: spy.stop },
            audioDataPromise: Promise.resolve({}),
          }),
        },
        '../../../../scenes': {},
        '../../../../store': { state: {} },
      });

      await method.call({ subtitles: {}, animationProgress: spy.animationProgress });
      assert(spy.finishedPromise.called);
      assert(spy.postMessage.called);
      assert(spy.stop.called);
      assert(spy.animationProgress.called);
    });
  });
});

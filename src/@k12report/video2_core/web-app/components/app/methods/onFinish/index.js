const { subtitles, audio } = require('../../../../modules');
const audioMetadata = require('../../../../modules/audio/metadata');
const scenes = require('../../../../scenes');
const { state } = require('../../../../store');

module.exports = async function(scene) {
  // console.debug('All Finished', { scene });
  const { mediaRecorder, audioDataPromise, mediaRecordStartTime } = audio();
  try {
    mediaRecorder.stop();
  } catch (error) {}
  const audioData = await audioDataPromise;
  if (window.puppeteer) {
    window.puppeteer.finished = true;
    window.puppeteer.finishedPromise.resolve();
    window.puppeteer.audioData = audioData;
    window.puppeteer.audioMetadata = audioMetadata.data;
    if (this.subtitles) {
      window.puppeteer.subtitles = {
        default: subtitles(this.subtitles.default, mediaRecordStartTime),
        debug: subtitles(this.subtitles.debug, mediaRecordStartTime),
      };
    }
  }
  // console.log(subtitles(this.subtitles, mediaRecordStartTime));
  this.finished = true;
  if (state.showFinishedMessage) {
    this.setState({ finished: true });
  }

  /* To stop screencast */
  window.postMessage({ type: 'REC_STOP' }, '*');

  this.animationProgress({ finished: true, scenes });

  console.log('Fin');
};

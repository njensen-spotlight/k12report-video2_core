exports.app = require('./app');
exports.captions = require('./captions');
exports.controls = require('./controls');
exports.debug = require('./debug');
exports.parallelScenesLoader = require('./parallelScenesLoader');
exports.sceneLoader = require('./sceneLoader');
exports.sequentialScenesLoader = require('./sequentialScenesLoader');
exports.startButton = require('./startButton');

try {
  Object.assign(exports, require(__WEB_APP_DIR__ + '/components'));
} catch (error) {
  console.warn('No author components found');
}

const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const { shallow } = require('enzyme');
const sinon = require('sinon');
const h = require('hyperchain/react')();
const t = require('../../../tests/utils');

describe(__dirname, () => {
  it('output', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [{ component: sceneComponent }];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');
  });
  it('single scene', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [{ component: sceneComponent }];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    const sceneLoaderLastCallProps = sceneLoader.lastCall.args[0];
    assert(sceneLoaderLastCallProps.scene);
    assert(!sceneLoaderLastCallProps.scenes);
    assert.equal(sceneLoaderLastCallProps.scene, scenes[0]);
  });
  it('nested scenes', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [
      [{ component: sceneComponent }]
    ];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    const sceneLoaderLastCallProps = sceneLoader.lastCall.args[0];
    assert(!sceneLoaderLastCallProps.scene);
    assert(sceneLoaderLastCallProps.scenes);
    assert.deepEqual(sceneLoaderLastCallProps.scenes, scenes[0]);
  });
  it('onFinish', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [{ component: sceneComponent }];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    assert.deepEqual(Array.from(wrapper.instance().scenesLeft), scenes);
    assert.equal(wrapper.instance().isParentFinished, false);

    const sceneLoaderLastCall = sceneLoader.lastCall.args[0];
    sceneLoaderLastCall.onFinish(scenes[0]);
    assert.deepEqual([...wrapper.instance().scenesLeft], []);
    assert.equal(wrapper.instance().state.isParentFinished, true);
    assert.equal(sceneLoaderLastCall.isParentFinished(), true);

    const propsOnFinishLastCall = props.onFinish.lastCall.args[0];
    assert.deepEqual(propsOnFinishLastCall, scenes);
  });
  it('onFinish multiple scenes', async () => {
    const scenes = [{ component: sinon.spy(() => 'scene1') }, { component: sinon.spy(() => 'scene2') }];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoadersceneLoader');

    assert.deepEqual(Array.from(wrapper.instance().scenesLeft), scenes);
    assert.equal(wrapper.instance().isParentFinished, false);

    let sceneLoaderLastCall;
    sceneLoaderLastCall = sceneLoader.lastCall.args[0];
    sceneLoaderLastCall.onFinish(scenes[0]);
    assert.deepEqual([...wrapper.instance().scenesLeft], [scenes[1]]);
    // assert.equal(wrapper.instance().isParentFinished, false);
    // assert.equal(sceneLoaderLastCall.isParentFinished(), false);

    sceneLoaderLastCall = sceneLoader.lastCall.args[0];
    sceneLoaderLastCall.onFinish(scenes[1]);
    assert.deepEqual([...wrapper.instance().scenesLeft], []);
    // assert.equal(wrapper.instance().state.isParentFinished, true);
    // assert.equal(sceneLoaderLastCall.isParentFinished(), true);

    const propsOnFinishLastCall = props.onFinish.lastCall.args[0];
    assert.deepEqual(propsOnFinishLastCall, scenes);
  });
  it('onFinish nested scenes', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scenes = [[{ component: sceneComponent }]];
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // 'root/data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sceneLoader');

    assert.deepEqual(Array.from(wrapper.instance().scenesLeft), scenes);
    assert.equal(wrapper.instance().isParentFinished, false);

    const sceneLoaderLastCall = sceneLoader.lastCall.args[0];
    sceneLoaderLastCall.onFinish(scenes[0]);
    assert.deepEqual([...wrapper.instance().scenesLeft], []);
    // assert.equal(wrapper.instance().isParentFinished, true);
    // assert.equal(sceneLoaderLastCall.isParentFinished(), true);

    const propsOnFinishLastCall = props.onFinish.lastCall.args[0];
    assert.deepEqual(propsOnFinishLastCall, scenes);
  });
});

const { Component } = require('react');
const { createRef } = require('react');
const h = require('hyperchain/react')();
const { walkScenes } = require('../../utils');

let counter = 0;

module.exports = class extends Component {
  componentWillMount() {
    // console.debug(`Loading ${this.props.scenes.length} scenes in parallel:`, this.props.scenes.map(s => s.scene || `[...${s.length} scenes in sequence]`).join(', '));
    this.ref = createRef();
    this.ref.current = {
      scenesLeft: new Set(this.props.scenes),
      isParentFinished: false
    };
  }
  render() {
    const { sceneLoader } = require('..');

    return h._({ key: counter++ }, this.props.scenes.map((scene, i) => h(sceneLoader, {
      ...this.props,
      key: i,
      scene: null,
      scenes: null,
      [Array.isArray(scene) ? 'scenes' : 'scene']: scene,
      onFinish: (scene) => {
        this.ref.current.scenesLeft.delete(scene);
        let endAll;
        if (Array.isArray(scene)) {
          walkScenes(scene, scene => {
            endAll = endAll || scene.endAll;
          });
        }

        if (
          (scene && scene.endSiblings || scene.endAll)
          || endAll
          || !Array.from(this.ref.current.scenesLeft).find(s => !s.endWithOthers)
          || !this.ref.current.scenesLeft.size
        ) {
          if (!this.ref.current.isParentFinished) {
            this.props.onFinish(this.props.scenes);
            this.ref.current.isParentFinished = true;
          }
        }

        // console.debug('End parallel  :', scene, { endAll });
      },
      parallel: false,
      isParentFinished: () => this.ref.current.isParentFinished !== false,
    })));
  }
};

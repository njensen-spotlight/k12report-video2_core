const h = require('hyperchain/react')({ style: require('./index.styl') });

module.exports = (props) => {
  // return 'hi';
  const play = () => {
    props.paused.resume();
    delete props.paused;
  };
  const pause = () => {
    let resume;
    props.paused = window.paused = new Promise((_) => { resume = _; });
    props.paused.resume = resume;
  };
  const back = () => {
    props.scene = 'greeting';
  };
  const next = () => {
    props.scene = 'table';
  };
  const stepForward = () => {
    props.scene = 'table';
  };

  return h.div.controls(
    // h.button.back({ onClick: back }, '⏪'),
    props.paused
      ? h.button.play({ onClick: play }, '⏵')
      : h.button.pause({ onClick: pause }, '⏸'),
    props.paused && h.button.stepForward({ onClick: stepForward }, '⧐'),
    // h.button.next( { onClick: next }, '⏩'),
  );
};

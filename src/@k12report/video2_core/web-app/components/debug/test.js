const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const { shallow } = require('enzyme');
const h = require('hyperchain/react')();
const t = require('../../../tests/utils');

describe(__dirname, () => {
  it('should work', () => {
    const result = shallow(h(proxyquire('.', {
      // 'root/data': {  },
      ...t.mockStore(),
    })));
    assert.equal(result.text(), '{}\n');
  });
});

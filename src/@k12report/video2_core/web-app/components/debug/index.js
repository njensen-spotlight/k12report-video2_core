const h = require('hyperchain/react')({ style: require('./index.styl'), tagClass: true });
const { view, debug } = require('../../store');
const YAML = require('yaml');

module.exports = view((props) => {
  return h.pre.debug(YAML.stringify(JSON.parse(JSON.stringify({
    ...JSON.parse(JSON.stringify(debug)),
    // ...debug,
    // ...props,
    // test: {test: {test: {test: 1}}},
    // captions: props.captions,
    // puppeteer: context.puppeteer,
    // progression: props.progression,
    // progression,
    // progressionFull: props.progression,
    // context: Object.keys(context),
    // props,
    // context
  }))));
});

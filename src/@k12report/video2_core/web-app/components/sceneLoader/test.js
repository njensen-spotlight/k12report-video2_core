const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const { shallow } = require('enzyme');
const sinon = require('sinon');
const h = require('hyperchain/react')();
const t = require('../../../tests/utils');

describe(__dirname, () => {
  it.skip('output', async () => {
    const sceneComponent = sinon.spy(() => 'scene');
    const scene = { component: sceneComponent };
    const sceneLoader = sinon.spy(() => 'sceneLoader');
    const stubs = {
      '..': { sceneLoader },
      // '../../data': {  },
      ...t.mockStore(),
    };
    const props = {
      scene,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'scene');
  });
  it('single scene', async () => {
    const scene = { component: sinon.spy(() => 'scene') };
    const sequentialScenesLoader = sinon.spy(() => 'sequentialScenesLoader');
    const parallelScenesLoader = sinon.spy(() => 'parallelScenesLoader');
    const stubs = {
      '..': { sequentialScenesLoader, parallelScenesLoader },
      '../../modules': { animate: sinon.spy(() => {}) },
      '../../utils': { forAwaitOf: sinon.spy(() => {}) },
      // '../../data': {  },
      ...t.mockStore(),
    };
    const props = {
      scene,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'scene');

    assert(sequentialScenesLoader.notCalled);
    assert(parallelScenesLoader.notCalled);
  });
  it('multi scene parallelScenesLoader', async () => {
    const scenes = [{ component: sinon.spy(() => 'scene') }];
    const sequentialScenesLoader = sinon.spy(() => 'sequentialScenesLoader');
    const parallelScenesLoader = sinon.spy(() => 'parallelScenesLoader');
    const stubs = {
      '..': { sequentialScenesLoader, parallelScenesLoader },
      // '../../data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'parallelScenesLoader');

    assert(sequentialScenesLoader.notCalled);
    assert(parallelScenesLoader.called);
  });
  it('multi scene sequentialScenesLoader', async () => {
    const scenes = [{ component: sinon.spy(() => 'scene') }];
    const sequentialScenesLoader = sinon.spy(() => 'sequentialScenesLoader');
    const parallelScenesLoader = sinon.spy(() => 'parallelScenesLoader');
    const stubs = {
      '..': { sequentialScenesLoader, parallelScenesLoader },
      // '../../data': {  },
      ...t.mockStore(),
    };
    const props = {
      scenes,
      onFinish: sinon.spy(),
      parallel: false,
    };
    const component = proxyquire('.', stubs);
    const wrapper = shallow(h(component, props));

    assert.equal(wrapper.html(), 'sequentialScenesLoader');

    assert(sequentialScenesLoader.called);
    assert(parallelScenesLoader.notCalled);
  });
  it('throws when no scene', async () => {
    const sequentialScenesLoader = sinon.spy(() => 'sequentialScenesLoader');
    const parallelScenesLoader = sinon.spy(() => 'parallelScenesLoader');
    const stubs = {
      '..': { sequentialScenesLoader, parallelScenesLoader },
      // '../../data': {  },
      ...t.mockStore(),
    };
    const props = {};
    const component = proxyquire('.', stubs);
    assert.throws(() => shallow(h(component, props)));
  });
});

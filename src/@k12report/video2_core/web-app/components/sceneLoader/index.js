const h = require('hyperchain/react')();
const { Component, createRef } = require('react');
const { animate } = require('../../modules');
const _ = require('../../utils');

let scenesLoaded = 0;
let counter = 0;

module.exports = class extends Component {
  constructor() {
    super();
    this.state = {};
    this.sceneComponent = createRef();
  }

  render() {
    const { sequentialScenesLoader, parallelScenesLoader } = require('..');

    if (this.props.scene) {
      return h.div.sc({ key: counter++, ref: ref => this.sceneComponent.current = ref || this.sceneComponent.current },
        h(this.props.scene.component),
      );
    } else if (this.props.scenes) {
      if (this.props.parallel !== false) {
        return h(parallelScenesLoader, { ...this.props, key: counter++ });
      } else {
        return h(sequentialScenesLoader, { ...this.props, key: counter++ });
      }
    } else {
      throw new Error('Need props[scene|scenes]');
    }
  }

  componentDidMount() {
    this.animate();
  }

  componentDidUpdate(props) {
    if (this.props.scene !== props.scene) {
      // this.animate();
    }
  }

  async animate() {
    if (!this.props.scene) return;
    scenesLoaded++;
    await _.forAwaitOf(animate.call(this), animateIterator => this.props.animationProgress({
      animateIterator,
      isParentFinished: this.props.isParentFinished(),
      scene: this.props.scene,
      scenesLoaded,
    }));
    scenesLoaded--;
    this.props.onFinish(this.props.scene);
  }
};

const h = require('hyperchain/react')({ style: require('./index.styl'), tagClass: true });
const data = require('../../data');
const { view, state } = require('../../store');

/* Most likely overridden by author app */

module.exports = view((props) => {
  if (!data.metadata.showCaptions) return;
  const captions = state.captions || props.captions;

  return h.div.captions({
    length: captions.length
  }, captions);
});

const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const { shallow } = require('enzyme');
const h = require('hyperchain/react')();
const t = require('../../../tests/utils');

describe(__dirname, () => {
  it('should work', () => {
    const result = shallow(h(proxyquire('.', {
      ...t.mockStore(),
      ...t.mockData(),
    })));
    assert.equal(result.html(), null);
  });
  it('should work', () => {
    const result = shallow(h(proxyquire('.', {
      ...t.mockData({ metadata: { showCaptions: true } }),
      ...t.mockStore({ captions: 'captions' }),
    })));
    assert.equal(result.html(), '<div length="8" class="div captions">captions</div>');
  });
});

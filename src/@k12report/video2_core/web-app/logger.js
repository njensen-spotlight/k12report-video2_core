module.exports = require('pino')({
  level: 'trace',
  browser: {
    write: typeof pinoPuppeteerLink === 'function' ? new Proxy({}, {
      get: (t, key) => (...msg) => pinoPuppeteerLink(key, ...msg)
    }) : console.debug.bind(console)
  }
});

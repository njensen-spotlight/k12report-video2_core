const Path = require('path');
const fs = require('fs-extra');
const { format } = require('util');

const output = exports.output = { file: 'output.log' };

exports.patch = outputDir => {
  output.file = Path.join(outputDir, 'output.log');
  patch('stdout');
  patch('stderr');
};

const logToFile = exports.logToFile = (str, std = 'stdout', file = output.file) => {
  if (Array.isArray(str)) str = format(...str) + '\n';
  const time = (new Date).toISOString();
  fs.appendFileSync(file, `[${time}] [${std}] ${str}`);
};

function patch(std) {
  const write = process[std].write;
  process[std].write = (str) => {
    write.call(process[std], str);
    logToFile(str, std);
  };
}

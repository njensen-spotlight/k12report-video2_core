const config = require('../config');
const logger = require('./logger');

module.exports = new class TimeDiff {
  constructor({ logger = console.log.bind(console) } = {}) {
    this.logger = logger;
    this.labels = {};
    this.startTime = +new Date;
  }
  start(label) {
    if (!this.labels[label]) this.labels[label] = { diffs: [] };
    this.labels[label].then = +new Date;
  }
  end(label) {
    label = this.labels[label];
    const diff = this.now - label.then;
    label.diffs.push(diff);
    this.log();
  }
  restart(label) {
    if (this.labels[label]) {
      this.end(label);
      this.start(label);
    } else {
      this.start(label);
    }
    this.log();
  }
  log(...msg) {
    if (!(config.timeDiff || config.debug)) return;
    const timeDiff = {};
    for (const label in this.labels) {
      const { diffs } = this.labels[label];
      timeDiff[label] = {
        averageLast3: Math.ceil(add(diffs.slice(-3)) / 3),
        average: Math.ceil(add(diffs) / diffs.length),
      };
    }
    logger.debug({ timeDiff });
  }
  get now() {
    return +new Date;
  }
};

function add(array) {
  let result = 0;
  for (const num of array) {
    result += num;
  }
  return result;
}

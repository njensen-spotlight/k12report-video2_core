/**
 * Custom Webpack Plugin to convert Media files (mp3/mp4) to WebM for Puppeteer
 */

const Path = require('path');
const OS = require('os');
const fs = require('fs');
const Ffmpeg = require('fluent-ffmpeg');
const loaderUtils = require('loader-utils');
// const urlLoader = require('url-loader');
const urlLoader = require('file-loader');
const uuid = require('uuid/v1');
const memoize = require('./memoize');

const console = new Proxy({}, { get: (t, level) => (...msg) => global.console[level]('[webm]', ...msg) });

let waitForLast = Promise.resolve();

module.exports = exports;
exports.raw = true;

async function exports(source) {
  const options = loaderUtils.getOptions(this) || {};

  if (options.disable) return urlLoader.call(this, source);

  const context = this.rootContext || this.options.context;
  const { resourcePath } = this;
  const callback = this.async();

  waitForLast = waitForLast.then(async () => {
    try {
      const outputContent = Buffer.from(await memoize(convert)({ context, resourcePath, source }));
      callback(null, urlLoader.call(this, outputContent));
    } catch (error) {
      callback(error);
      throw error;
    }
  });
}

function convert({ context, resourcePath }) {
  const output = Path.join(OS.tmpdir(), 'webpack-webm_' + uuid());
  const relativeFileName = Path.relative(context, resourcePath);

  console.log(`Converting '${relativeFileName}'`);
  const ffmpeg = Ffmpeg({ logger: console });
  ffmpeg.input(resourcePath);
  ffmpeg.format('webm');
  ffmpeg.output(output);
  ffmpeg.run();
  return Promise.race([
    new Promise(r => ffmpeg.once('end', r)),
    new Promise((r, x) => ffmpeg.once('error', x)),
  ]).then(() => {
    // console.log(`Converted '${relativeFileName}'`);
    const outputContent = fs.readFileSync(output);
    fs.unlinkSync(output);
    return outputContent;
  }).catch(e => {
    console.error(`Error converting '${relativeFileName}'`, e.message);
    try { fs.unlinkSync(output); } catch (error) {}
    throw e;
  });
}
const Memoize = require('memoize-fs')({ cachePath: '.cache' });

module.exports = (fn, argsProcessor = (...args) => args) => {
  const memoized = Memoize.fn(fn);
  return (...args) => memoized.then(fn => fn(...argsProcessor(...args)));
};

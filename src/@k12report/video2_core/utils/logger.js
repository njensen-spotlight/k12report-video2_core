const Path = require('path');
const pino = require('pino');
const fs = require('fs-extra');
const config = require('../config');

const logger = module.exports = create();

logger.info('Pino Logger started');

if (config.debug) {
  require('pino-debug')(create({ path: 'pino.debug.log' }), {
    auto: false,
    map: { '*': 'trace' }
  });
  patch('stdout');
  patch('stderr');
}

function create({
  enabled = Boolean(config.debug),
  level = 'trace',
  path = 'pino.log',
  destination: dest = createDestination(path),
} = {}) {
  return pino({
    enabled,
    level,
  }, dest);
}

function patch(std) {
  const write = process[std].write;
  process[std].write = (str) => {
    write.call(process[std], str);
    if (std === 'stdout') {
      logger.info(str);
    } else {
      logger.error(str);
    }
  };
}

function createDestination(filename) {
  const path = Path.join(config.outputDir, filename);
  fs.ensureDirSync(config.outputDir);
  return pino.destination(path);
}

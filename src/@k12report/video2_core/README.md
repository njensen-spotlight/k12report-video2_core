# video2_core

Video viewer, editor, processor, and generation framework.

Please see **[Wiki]** for more info.

Please see the accompanying **[video2_author]** project.

[wiki]: ../../wiki
[video2_author]: ../../../video2_author

const enzyme = require('enzyme');
const enzymeAdapter = require('enzyme-adapter-react-16');
const ignoreStyles = require('ignore-styles').default;
const chain = require('chain-free');
// const proxyquire = require('proxyquire')

enzyme.configure({ adapter: new enzymeAdapter() });

ignoreStyles(['.styl']);

global.document = chain(() => () => {});
global.navigator = chain(() => () => {});
global.window = chain(() => () => {});
global.AudioContext = class { constructor() { return chain(() => () => {}); } };
global.MediaRecorder = class { constructor() { return chain(() => () => {}); } };
global.chrome = chain(() => () => {});
global.history = chain(() => () => {});
global.location = { href: 'http://localhost' };
global.EventTarget = new Function;

global.__WEB_APP_DIR__ = '__WEB_APP_DIR__';

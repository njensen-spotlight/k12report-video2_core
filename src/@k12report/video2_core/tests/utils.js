const chain = require('chain-free');

const t = exports;

t.identityGenerator = function*(i) { return i; };

t.asyncIteratorToArray = async it => {
  const array = [];
  for await (const value of it) {
    array.push(value);
  }
  return array;
};

let unmock = () => {};
beforeEach(() => unmock());
afterEach(() => unmock());
t.mockGlobals = globals => {
  const backup = {};
  for (const key in globals) {
    backup[key] = global[key];
    global[key] = globals[key];
  }
  unmock = () => {
    for (const key in backup) {
      global[key] = backup[key];
    }
    unmock = () => {};
  };
};

t.mockH = (result = []) => ({
  'hyperchain/react': () => chain((...args) => {
    result.push(args);
  }, {}, {
    base: (...args) => {
      result.push(args);
    }
  })
});
t.mockStore = state => ({
  [require.resolve('../web-app/store')]: {
    view: i => i,
    state: { ...state },
    '@noCallThru': true,
  }
});
t.mockData = data => ({
  [require.resolve('../web-app/data')]: {
    metadata: data && data.metadata || {},
    ...data,
  }
});

t.throws = () => { throw new Error('Shouldn\'t have been called'); };

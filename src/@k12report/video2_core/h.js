const H = require('hyperchain/react');
const opts = require('hyperchain/opts');

opts.tagClass = true;
opts.stylePreserveNames = true;
// opts.styleOmitUnused = true;

module.exports = H;

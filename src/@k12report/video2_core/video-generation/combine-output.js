const Path = require('path');
const fs = require('fs-extra');
const ffmpeg = require('./ffmpeg');

module.exports = async ({
  audio,
  video,
  outputDir,
  frameRate = 25,
  fadeout,
}) => {

  const outputPath = Path.join(outputDir, 'output.mp4');

  await ffmpeg(async ffmpeg => {
    ffmpeg.input(video.output + '/%d.png');
    ffmpeg.inputFps(frameRate);
    // ffmpeg.inputOptions('-itsoffset', 2);
    ffmpeg.input(audio.output);
    ffmpeg.outputFps(frameRate);
    ffmpeg.outputOptions('-pix_fmt', 'yuv420p');
    ffmpeg.outputOptions('-vf', 'colormatrix=bt601:bt709');
    if (fadeout) {
      const totalFrames = await fs.readdir(video.output);
      const duration = totalFrames.length / frameRate * 1000;
      const fadeStart = duration - fadeout;
      ffmpeg.outputOptions('-af', `afade=out:st=${fadeStart/1000}:d=${fadeout/1000}`);
    }
    ffmpeg.output(outputPath);
    return { log: 'Combining...' };
  });

  return outputPath;
};

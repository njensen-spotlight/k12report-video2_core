const ffmpeg = require('./ffmpeg');
const ffprobe = require('./ffprobe');
const { renameFile } = require('./utils');

module.exports = detectSilence;

async function detectSilence(input, {
  maxSilenceStart = 0,
  silenceThreshold = 0.02,
} = {}) {

  const output = renameFile(input, o => o + '_detect-silence');
  // const audioDuration = probeOutput.sanitized.audio.duration;
  await ffmpeg(async ffmpeg => {
    ffmpeg.input(input);
    ffmpeg.outputOptions('-vcodec', 'copy');
    ffmpeg.outputOptions('-af', `silenceremove=1:0:${silenceThreshold}:0:all:0:1:0:1:all`);
    ffmpeg.output(output);
    return { log: 'Detecting silence...' };
  });

  const [inputProbe, outputProbe] = await Promise.all([
    ffprobe(input),
    ffprobe(output),
  ]);

  const original = inputProbe.sanitized.audio.duration;
  const silenceRemoved = outputProbe.sanitized.audio.duration;
  const diff = Math.abs(original - silenceRemoved);
  // console.log('Silence in the audio at the beginning:', diff);
  if (diff > maxSilenceStart) {
    throw new Error(`Silence at start larger than allowed (${diff} > ${maxSilenceStart})`);
  }
}


/* notes
ffmpeg -i output.mp4 -af silencedetect=noise=0.0001 -f null -
ffmpeg -i that.mp4 -vcodec copy -af silenceremove=start_periods=1:start_threshold=0.02 that_silence-removed.mp4
*/

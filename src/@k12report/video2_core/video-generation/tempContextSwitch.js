const Path = require('path');
const fs = require('fs');
const os = require('os');
const crypto = require('crypto');

const tmpdir = os.tmpdir();
const randomText = +new Date + String(crypto.randomBytes(4).readUInt32LE(0));
const tmpfile = Path.join(tmpdir, 'video_core--temp-context-switch_' + randomText);

exports.write = () => {
  fs.writeFileSync(tmpfile, 'Hack to force context-switch');
};

exports.cleanup = () => {
  fs.unlinkSync(tmpfile);
};

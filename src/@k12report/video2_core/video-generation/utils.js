const Path = require('path');
const config = require('../config');
const tempContextSwitch = require('./tempContextSwitch');
const logger = require('../utils/logger');

const _ = exports;

_.THROTTLED_LOG_DELAY = { delay: 3 * 60 * 1000 };

let lastLog = 0;
_.throttledLog = (...msg) => {
  const now = parseInt(new Date() / 1000);
  const diff = now - lastLog;
  if (config.debug || config.verbose || diff > config.throttledLogDelay) {
    console.log(...msg);
    lastLog = now;
  } else {
    logger.info(...msg);
  }
  if (config.tempContextSwitch) {
    tempContextSwitch.write();
  }
};

_.getLevel = type => {
  switch (type) {
  case 'error':
  case 'warn':
    return 0;
  case 'debug':
    return 1;
  case 'info':
    return 2;
  case 'log':
    return 3;
  default:
    return 4;
  }
};

_.getNumericVerboseLevel = verboseLevel => {
  if (!verboseLevel) return 0;
  if (verboseLevel === true) return 1;
  if (Array.isArray(verboseLevel)) return verboseLevel.length;
  return 3;
};

_.promisify = fn => new Promise((resolve, reject) => fn((err, data) => err ? reject(err) : resolve(data)));

_.try = (fn, onError = e => console.error(e.message)) => {
  try {
    const res = fn();
    if (res && res.then) {
      return res.catch(onError);
    } else {
      return res;
    }
  } catch (error) {
    return onError(error);
  }
};

_.renameFile = (input, newName) => {
  const dirname = Path.dirname(input);
  const extname = Path.extname(input);
  const basename = Path.basename(input, extname);
  if (typeof newName === 'function') {
    newName = newName(basename, { extname, dirname });
  }
  if (typeof newName !== 'string') {
    throw new Error('Invalid newName');
  }
  if (!newName.includes(extname)) {
    newName = newName + extname;
  }
  if (!Path.isAbsolute(newName)) {
    newName = Path.join(dirname, newName);
  }
  return newName;
};

require('./panic');
require('./first');
const fs = require('fs-extra');
const ms = require('ms');
const Path = require('path');
const { panic } = require('./panic');
const captureVideo = require('./capture-video');
const captureScreencast = require('./capture-screencast');
const captureAudio = require('./capture-audio');
const captureFramesFactory = require('./capture-frames');
const { closeAll } = require('./puppeteer');
const combineOutput = require('./combine-output');
const adjustLength = require('./adjust-length');
const maxDurationDiff = require('./max-duration-diff');
const maxSilenceStart = require('./max-silence-start');
const data = require('../web-app/data');
const { THROTTLED_LOG_DELAY } = require('./utils');

/* Run the main function, then exit cleanly, or display error and exit with a code */
module.exports = config => main(config).then(() => process.exit(0)).catch(panic);

async function main(config) {

  const options = { ...config };
  const { outputDir, buildDir, webAppUrl, concurrency } = options;

  const startedAt = new Date();

  // await fs.emptyDir(outputDir);
  // await fs.ensureDir(outputDir);

  const frameRate = options.frameRate = options.frameRate || (data.metadata.frameRate || data.metadata.framerate);

  const { captureFrame, writeMetaData: writeFramesMetaData } = await captureFramesFactory(options);

  if (options.outputFrames) {
    options.captureFrame = captureFrame;
  }

  let output;

  if (config.screencast) {
    console.log('Capturing audio and video via screencast...');
    output = await captureScreencast(options);
  } else {
    let video;
    let audio;
    if (concurrency) {
      console.log('Capturing audio and video...');
      [video, audio] = await Promise.all([
        config.audioOnly || captureVideo(options),
        captureAudio(options),
      ]);
    } else {
      console.log('Capturing video...');
      video = config.audioOnly || await captureVideo(options);
      console.log('Capturing audio...');
      audio = await captureAudio({ ...options, video });
    }

    options.audio = audio;
    options.video = video;

    console.log('Capturing finished', `(in ${ms(new Date - startedAt)})`);

    closeAll();

    if (config.audioOnly) {
      // No need to combine
    } else if (config.combine) {
      const combinationStartedAt = new Date;
      output = await combineOutput(options);
      console.log('Combination finished', `(in ${ms(new Date - combinationStartedAt)})`);
    }

    let framesToSave = [];
    if (writeFramesMetaData) {
      const framesMetaData = await writeFramesMetaData();
      framesToSave = framesMetaData.frames.map(f => f.path);
    }

    await Promise.all([
      config.viewer && viewer({ outputDir }).catch(console.error),
      config.combine && config.cleanup && cleanup({ buildDir, audio, video, framesToSave }).catch(e => console.warn('Cleanup error:', e.message)),
      config.combine && !config.audioOnly && config.maxDurationDiff && maxDurationDiff(output, config),
    ]);

    if (config.audioOnly) {
      // No need
    } else if (config.combine) {
      await maxSilenceStart(output, config);

      if (config.adjustLength) {
        output = await adjustLength(output, config);
      }

      config.maxDurationDiff && await maxDurationDiff(output, config);
    }
  }

  console.log('Done', `(in ${ms(new Date - startedAt)})`);
  console.log('Output:', output);
}

function viewer({ outputDir }) {
  return fs.copy(Path.join(__dirname, 'viewer.html'), Path.join(outputDir, 'viewer.html'));
}

async function cleanup({ buildDir, audio, video, framesToSave }) {
  console.log('Cleaning up...');
  return Promise.all([
    fs.remove(buildDir),
    fs.remove(audio.output),
    cleanupFrames(video.output, framesToSave)
  ]);
}

async function cleanupFrames(framesDir, framesToSave) {
  const totalFrames = (await fs.readdir(framesDir)).map(r => Path.join(framesDir, r));
  const framesToDelete = totalFrames.filter(f => !framesToSave.includes(f));
  // console.log({ framesToSave, framesToDelete });
  return Promise.all(framesToDelete.map(f => fs.remove(f)));
}

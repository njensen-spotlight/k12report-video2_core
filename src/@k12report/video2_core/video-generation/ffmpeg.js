const Ffmpeg = require('fluent-ffmpeg');
const { throttledLog } = require('./utils');

module.exports = async (opts, cb) => {
  if (!cb) {
    [cb, opts] = [opts, {}];
  }
  if (typeof cb !== 'function') {
    throw new Error('Need a function');
  }
  /* Initialize ffmpeg */
  const ffmpeg = Ffmpeg({ logger: console.log, ...opts });
  let ret = await cb(ffmpeg);
  ret = ret || {}
  let lastLog = ret.log || 'Running FFMPEG...';

  ffmpeg.on('start', console.log);
  console.log(lastLog);

  ffmpeg.run();

  ffmpeg.on('progress', _ => lastLog = Object.entries(_).map(_ => _.join(': ')).join(', '));
  const logInterval = setInterval(() => throttledLog(lastLog), 1000);

  /* Wait till ffmpeg ends (or errors) */
  try {
    await Promise.race([
      new Promise(_ => ffmpeg.once('end', _)),
      new Promise((_, x) => ffmpeg.once('error', x)),
    ]);
  } finally {
    clearInterval(logInterval);
  }
}

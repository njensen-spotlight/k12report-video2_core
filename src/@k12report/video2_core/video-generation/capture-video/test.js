const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const sinon = require('sinon');
const t = require('../../tests/utils');

describe(__dirname, () => {

  const prep = () => {
    const globals = {
      window: { puppeteer: {} }
    };
    const spy = {
      fs: {
        createWriteStream: sinon.spy(i => i),
      },
      stream: {
        push: sinon.spy(),
        pipe: sinon.spy(i => i),
        once: sinon.spy(((e, h) => e !== 'error' && h())),
      },
      puppeteer: sinon.spy(() => ({ page: spy.page })),
      page: {
        evaluate: sinon.spy((e, ...args) => e(...args)),
        evaluateOnNewDocument: sinon.spy((e, ...args) => e(...args)),
        exposeFunction: sinon.spy(),
        screenshot: sinon.spy(),
      },
    };
    t.mockGlobals(globals);
    const fn = proxyquire('.', {
      'fs-extra': {
        emptyDir: () => {},
        createWriteStream: spy.fs.createWriteStream,
      },
      path: {
        join: (...paths) => paths.join('/'),
      },
      stream: {
        Readable: function() { return spy.stream; }
      },
      'p-defer': require('p-defer'),
      '../puppeteer': { puppeteer: spy.puppeteer, }
    });
    return { spy, globals, fn };
  };

  it('basic', async () => {
    const { spy, globals, fn } = prep();
    const input = {
      outputDir: 'outputDir',
      webAppUrl: 'webAppUrl',
      // captureFrame: sinon.spy(),
    };
    const { output, scenes } = await fn(input);
    assert.equal(output, 'outputDir/video.tmp');
  });

  it('full', async () => {
    const { spy, globals, fn } = prep();
    const input = {
      outputDir: 'outputDir',
      webAppUrl: 'webAppUrl',
      // captureFrame: sinon.spy(),
    };
    const { output, scenes } = await fn(input);
    assert.equal(output, 'outputDir/video.tmp');
    assert(spy.puppeteer.calledOnceWith(input.webAppUrl));
    assert(spy.puppeteer.args[0][1].beforeClick);
    await spy.puppeteer.args[0][1].beforeClick({ page: spy.page });
    assert(spy.page.exposeFunction.calledOnceWith('PUPPETEER_waitForScreenshot'));
    assert(spy.page.exposeFunction.args[0][1]);
    const PUPPETEER_waitForScreenshot = spy.page.exposeFunction.args[0][1];

    assert(spy.stream._read);
    const promise = PUPPETEER_waitForScreenshot({});
    await spy.stream._read();
    await promise;
  });
});

const Path = require('path');
const fs = require('fs-extra');
const defer = require('p-defer');
const Puppeteer = require('../puppeteer');
const time = require('../../utils/time-diff');

module.exports = async (config) => {

  const {
    webAppUrl,
    outputDir,
    captureFrame,
  } = config;

  const output = Path.join(outputDir, 'frames');

  if (config.cache && await fs.exists(output)) {
    return { output }
  }

  await fs.emptyDir(output);

  let scenes;
  let counter = 0;
  let finished;
  const done = defer();
  let scenesLoadedCounter = 0;
  let scenesLoadedDefer = defer();
  const unresponsive = defer();
  let screenshotErrorsCounter = 0;

  const beforeClick = ({ page }) => page.exposeFunction('PUPPETEER_waitForScreenshot', async (result) => {
    unresponsive.flag = false;
    if (finished) return;
    if (result.finished) {
      done.resolve();
      finished = true;
    }
    if (result.scenes) {
      scenes = result.scenes;
    }
    if (++scenesLoadedCounter >= result.scenesLoaded) {
      scenesLoadedCounter = 0;
      try {
        const screenshotPath = Path.join(output, counter++ + '.png');
        await page.screenshot({ path: screenshotPath });

        const progress = await page.evaluate(() => window.puppeteer.progress);

        if (captureFrame) {
          captureFrame({ screenshotPath, scenes: progress });
        }
        time.restart('waitForScreenshot');
      } catch (error) {
        if (screenshotErrorsCounter++ > 5) {
          error.stack = 'Skipped too many screenshots. ' + error.stack;
          error.message = 'Skipped too many screenshots. ' + error.message;
          scenesLoadedDefer.reject(error);
          throw error
        } else {
          console.warn(`Skipped a screenshot`, error.message);
        }
      }
      scenesLoadedDefer.resolve();
      scenesLoadedDefer = defer();
      const { resolve } = scenesLoadedDefer;
      setTimeout(resolve, 1000);
    } else {
      return scenesLoadedDefer.promise;
    }
  });

  unresponsive.interval = setInterval(() => {
    if (unresponsive.flag) {
      console.error('Video capturing became unresponsive');
      unresponsive.reject('Video capturing became unresponsive');
    } else {
      unresponsive.flag = true;
    }
  }, 10000);

  try {
    await Puppeteer.puppeteer(webAppUrl, {
      ...config,
      label: 'video',
      // beforeGoto,
      beforeClick,
    });

    // await done.promise;
    await Promise.race([done.promise, unresponsive.promise]);
    return { output, scenes };
  } finally {
    clearInterval(unresponsive.interval);
  }
};

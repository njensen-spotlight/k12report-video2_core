const fs = require('fs-extra');
const Ffmpeg = require('fluent-ffmpeg');
const { promisify } = require('./utils');

module.exports = probe;

const cache = new Map();

async function probe(output, { useCache = !true, haltAtInvalidDuration = true } = {}) {
  if (useCache && cache.has(output)) {
    return cache.get(output);
  }
  console.log('Probing:', output);
  const { probe } = await ffprobe(output);
  const sanitized = probe.sanitized = {};
  for (const stream of probe.streams) {
    const { codec_type, duration: durationStr, ...rest } = stream;
    const durationNum = parseFloat(durationStr);
    if ((!durationStr || isNaN(durationNum)) && haltAtInvalidDuration) {
      throw new Error(`'${output}' stream '${codec_type}' has invalid duration: ${durationStr}`);
    }
    sanitized[codec_type] = {
      ...rest,
      durationStr,
      duration: durationNum,
    }
  }
  cache.set(output, probe);
  return probe;
}

async function ffprobe(input, ffmpeg = Ffmpeg(input)) {
  const probe = await promisify(cb => ffmpeg.ffprobe(cb));
  const write = (output = input + '.probe.json') => fs.writeFile(output, JSON.stringify(probe, null, 2));
  return { probe, write };
}

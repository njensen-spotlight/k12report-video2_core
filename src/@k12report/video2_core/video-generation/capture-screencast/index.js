const Path = require('path');
const fs = require('fs-extra');
const toBuffer = require('data-uri-to-buffer');
const Puppeteer = require('../puppeteer');

module.exports = async (config) => {

  const {
    webAppUrl,
    outputDir,
    // captureFrame,
    // waitBetweenScreenshots,
    // waitBetweenScreenshotsFor,
  } = config;

  const output = Path.join(outputDir, 'screencast.webm');

  const { page } = await Puppeteer.puppeteer(webAppUrl, {
    ...config,
    label: 'screencast',
    // beforeClick,
    headless: false,
    args: [
      '--enable-usermedia-screen-capturing',
      '--allow-http-screen-capture',
      '--no-sandbox',
      '--auto-select-desktop-capture-source=SCREENCAST',
      '--disable-setuid-sandbox',
      '--load-extension=' + __dirname,
      '--disable-extensions-except=' + __dirname,
    ],
  });

  const data = await page.evaluate(() => new Promise(_ => window.addEventListener('message', event => {
    if (event.data.type === 'download') _(event.data.data);
  })));

  fs.writeFileSync(output, toBuffer(data));

  return output;
};

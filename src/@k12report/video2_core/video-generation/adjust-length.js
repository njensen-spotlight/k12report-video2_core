const fs = require('fs-extra');
const ffmpeg = require('./ffmpeg');
const ffprobe = require('./ffprobe');
const { renameFile } = require('./utils');

module.exports = async (input, {
  adjustLength
} = {}) => {

  const original = renameFile(input, o => o + '_un-adjusted');
  await fs.rename(input, original);
  const output = input;
  input = original;

  const probeInput = await ffprobe(input);

  const videoDuration = probeInput.sanitized.video.duration;
  const audioDuration = probeInput.sanitized.audio.duration;
  const isVideoLonger = videoDuration > audioDuration;
  const isAudioLonger = audioDuration > videoDuration;
  const adjustAudio = adjustLength === 'audio';
  const adjustVideo = adjustLength === 'video';
  const adjustLong = adjustLength === 'long';
  const adjustShort = adjustLength === 'short';

  await ffmpeg(async ffmpeg => {
    ffmpeg.input(input);
    let log = 'Adjusting...';

    if (adjustAudio || (adjustLong && isVideoLonger) || (adjustShort && isAudioLonger)) {
      const ratio = audioDuration / videoDuration;
      ffmpeg.outputOptions('-af', `atempo=${ratio}`);
      ffmpeg.outputOptions('-vcodec', 'copy');
      log = `Adjusting audio to be ${ratio}x original length`;
    } else if (adjustVideo || (adjustLong && isAudioLonger) || (adjustShort && isVideoLonger)) {
      const ratio = videoDuration / audioDuration;
      ffmpeg.outputOptions('-vf', `setpts=(PTS-STARTPTS)/${ratio}`);
      ffmpeg.outputOptions('-acodec', 'copy');
      log = `Adjusting video to be ${ratio}x original length`;
    } else {
      throw new Error(`Invalid --adjustLength="${adjustLength}"`);
    }

    ffmpeg.output(output);

    return { log };
  });

  return output;
};

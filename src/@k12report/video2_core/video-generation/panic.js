const tasks = exports.tasks = new Set();

const panic = exports.panic = error => {
  console.error('FATAL', error.stack || error.message || error);
  Promise.all([...tasks].map(task => task())).catch(error => {
    console.error('FATAL', error.stack || error.message);
  });
  process.exitCode = 1;
  setTimeout(() => { /* Wait 1 second before exiting */ }, 1000);
};

'error,unhandledRejection,uncaughtException'.split(',').forEach(_ => process.on(_, panic));
'SIGINT,SIGTERM,SIGHUP'.split(',').forEach(signal => process.on(signal, () => panic(new Error(signal))));

const Puppeteer = require('puppeteer-core');
const config = require('../config');
// const debug = require('../utils/debug')('puppeteer');
const localChrome = require('./local-chrome');
const _ = require('./utils');
const panic = require('./panic');
// const data = require(__WEB_APP_DIR__ + '/data');
const data = require('../web-app/data');
const logger = require('../utils/logger');
const time = require('../utils/time-diff');

const browsers = new Set();

const closeAll = exports.closeAll = () => {
  console.log(`Closing ${browsers.size} browsers`);
  return Promise.all([...browsers].map(browser => browser.close()));
};

panic.tasks.add(closeAll);

exports.puppeteer = async (url, {
  evaluateOnNewDocument = () => {},
  beforeGoto = () => {},
  beforeClick = () => {},
  headless,
  devtools,
  chromePath: executablePath = localChrome,
  console: onConsole = () => {},
  label = '',
  debug: debugLevel,
  verbose: verboseLevel,
  args = [],
}) => {

  let done;

  if (!executablePath) {
    throw new Error('Cannot find local Chrome. Please make sure Chrome is installed locally. Or pass the path manually via --chrome-path');
  }


  const browser = await Puppeteer.launch({
    headless,
    // headless: false,
    devtools,

    userDataDir: process.env.PUPPETEER_USER_DATA_DIR,

    args: [
      '--no-sandbox',
      '--disable-web-security',
      '--enable-experimental-web-platform-features',
      ...args,
    ],

    timeout: 0,

    executablePath,

  });

  browsers.add(browser);
  [browser._close, browser.close] = [browser.close, () => {
    browsers.delete(browser);
    return browser._close();
  }];

  let [page] = await browser.pages();
  if (!page) page = await browser.newPage();

  page.setViewport({
    width: data.metadata.frameWidth || 800,
    height: data.metadata.frameHeight || 600,
  });

  const logBrowser = logger.child({ child: 'puppeteer' });
  await page.exposeFunction('pinoPuppeteerLink', (method, ...msg) => {
    // console.error({ method, msg });
    logBrowser[method](...msg);
  });

  await page.exposeFunction('timeStart', (l) => time.start(l));
  await page.exposeFunction('timeEnd', (l) => time.end(l));
  await page.exposeFunction('timeRestart', (l) => time.restart(l));

  /* Attach some variables to global `window` object, to let the web-app know it's being puppeteer-ed */
  await page.evaluateOnNewDocument((init = {}, config) => {
    window.puppeteer = init;
    window.puppeteer.finishedPromise = new Promise(resolve => setTimeout(() => {
      window.puppeteer.finishedPromise.resolve = resolve;
    }));
    window.__CONFIG__PUPPETEER__ = config;
  }, { label, debug: debugLevel }, config);

  if (evaluateOnNewDocument) {
    await page.evaluateOnNewDocument(evaluateOnNewDocument);
  }


  /* Handle web-app's console/error messages */
  const numericVerboseLevel = _.getNumericVerboseLevel(verboseLevel);
  page.on('console', async msg => {
    try {
      const text = _.try(() => msg.text(), e => e.message);
      const type = _.try(() => msg.type(), () => 'log');
      const args = await _.try(() => Promise.all(_.try(() => msg.args(), e => [{ jsonValue: () => e.message }]).map(j => {
        try {
          return j.jsonValue();
        } catch (error) {
          return `Couldn't read this console argument. ${error.message}`;
        }
      })), e => [e.message]);

      const log = () => (console[type] || console.log.bind(console, `[${type}]`))(`[${label}]`, ...args);

      if (type === 'error' || 'warn' === type || debugLevel) {
        log();
      } else if (!numericVerboseLevel) {
        _.throttledLog(`[${label}]`, ...args);
      } else if (_.getLevel(type) <= numericVerboseLevel) {
        log();
      } else {
        _.throttledLog(`[${label}]`, ...args);
      }

      if (onConsole) onConsole({ ...msg, text, type, args, label });
    } catch (error) {
      console.error('Puppeteer Console Error:', error.message);
    }
  });

  page.on('error', error => { throw error; });
  page.on('pageerror', error => { throw error; });
  page.on('disconnected', () => { throw new Error('Browser disconnected'); });

  if (beforeGoto) {
    await beforeGoto({ browser, page });
  }

  /* Open the web-app */
  await page.goto(url);

  if (beforeClick) {
    await beforeClick({ browser, page });
  }

  await page.waitFor('button#start-button');
  await page.click('button#start-button');

  /* Resolve the "ready" promise */
  // await page.evaluate(() => window.puppeteer_readyForFirstAnimation);
  await page.evaluate(() => window.readyForFirstAnimation);

  return { browser, page };
};

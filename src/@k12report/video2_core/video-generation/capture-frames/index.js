const fs = require('fs-extra');
const Path = require('path');

module.exports = async ({
  outputDir,
  frameRate = 25,
  outputFrames = { frameRate: Infinity },
}) => {

  if (!outputFrames || outputFrames.disable) return {};

  const framesMetaData = { frameRate, frames: [] };

  const cache = {};
  const progressString = (scenes, { slideIndex = true, currentTime = true } = {}) => Object.entries(scenes).map(([sceneName, sceneProps]) => `${sceneName}` + (slideIndex ? `[${sceneProps.slideIndex}]` : '') + (currentTime ? ` ${sceneProps.currentTime}` : '')).join(', ');

  const screenshots = [];
  let screenshotCounter = -1;
  const captureFrame = ({ screenshotPath: path, scenes }) => {
    screenshotCounter++;
    const last = screenshots[screenshots.length - 1];
    const push = (rest) => screenshots.push({ path, scenes, ...rest });
    const pushFill = (rest) => push({ path: last.path, ...rest });
    if (screenshotCounter === 0) {
      push();
    } else if (outputFrames.frameRate) {
      if (outputFrames.frameRate >= frameRate) {
        push();
      } else if (outputFrames.frameRate > 0) {
        if ((screenshotCounter % (frameRate / outputFrames.frameRate)) >= 1) {
          if (outputFrames.fill) {
            /* Skipping screenshot */
            pushFill();
          } else {
            /* Skipping entirely */
          }
        } else {
          push();
        }
      }
    } else if (outputFrames.scene) {
      const last = cache.lastProgressStringScene || '';
      const curr = progressString(scenes, { slideIndex: false, currentTime: false });
      cache.lastProgressStringScene = curr;
      if (last !== curr) {
        push();
      } else if (outputFrames.fill) {
        pushFill();
      }
    } else if (outputFrames.slide) {
      const last = cache.lastProgressStringSlide || '';
      const curr = progressString(scenes, { currentTime: false });
      cache.lastProgressStringSlide = curr;
      if (last !== curr) {
        push();
      } else if (outputFrames.fill) {
        pushFill();
      }
    } else {
      throw new Error(`Invalid --outputFrames=${JSON.stringify(outputFrames)}`);
    }
  };

  const writeMetaData = async () => {
    console.log('Writing screenshots...');
    for (const screenshot of screenshots) {
      framesMetaData.frames.push(screenshot);
    }
    await fs.writeFile(Path.join(outputDir, 'frames.json'), JSON.stringify(framesMetaData, null, 2));
    return framesMetaData;
  };

  return { captureFrame, writeMetaData };
};

const proxyquire = require('proxyquire');
const assert = require('assert');
const sinon = require('sinon');
const t = require('../../tests/utils');

describe(__dirname, () => {

  const prep = () => {
    const spy = {
      writeFile: sinon.spy(),
    };
    const fn = proxyquire('.', {
      'fs-extra': {
        emptyDir: () => {},
        writeFile: spy.writeFile,
      },
      path: {
        join: (...paths) => paths.join('/'),
      }
    });
    return { spy, fn };
  };

  it('basic', async () => {
    const { spy, fn } = prep();
    const input = {
      outputDir: 'outputDir',
      // outputFrames: {frameRate: },
    };
    const { captureFrame, screenshotPromises, writeMetaData } = await fn(input);

    // captureFrame({});
    captureFrame({ screenshotCounter: 1, screenshot: 'screenshot1' });
    captureFrame({ screenshotCounter: 2, screenshot: 'screenshot2' });
    await screenshotPromises;
    await writeMetaData();
    const writeFileCallArgs = spy.writeFile.getCalls().map(c => c.args);
    assert.deepEqual(writeFileCallArgs, [
      ['outputDir/frames/0.png', 'screenshot1'],
      ['outputDir/frames/1.png', 'screenshot2'],
      ['outputDir/frames.json', '{\n  "frameRate": 25,\n  "frames": [\n    {\n      "path": "outputDir/frames/0.png"\n    },\n    {\n      "path": "outputDir/frames/1.png"\n    }\n  ]\n}']
    ]);
  });

  it('--outputFrames=false', async () => {
    const { spy, fn } = prep();
    const input = {
      outputDir: 'outputDir',
      outputFrames: false,
    };
    assert.deepEqual(await fn(input), {});
  });

  it('--outputFrames > frameRate', async () => {
    const { spy, fn } = prep();
    const input = {
      outputDir: 'outputDir',
      frameRate: 1,
      outputFrames: { frameRate: 2 },
    };
    const { captureFrame, screenshotPromises, writeMetaData } = await fn(input);

    captureFrame({ screenshotCounter: 1, screenshot: 'screenshot1' });
    captureFrame({ screenshotCounter: 2, screenshot: 'screenshot2' });
    await screenshotPromises;
    await writeMetaData();
    const writeFileCallArgs = spy.writeFile.getCalls().map(c => c.args);
    // console.log(writeFileCallArgs);
    assert.deepEqual(writeFileCallArgs, [
      ['outputDir/frames/0.png', 'screenshot1'],
      ['outputDir/frames/1.png', 'screenshot2'],
      ['outputDir/frames.json', '{\n  "frameRate": 1,\n  "frames": [\n    {\n      "path": "outputDir/frames/0.png"\n    },\n    {\n      "path": "outputDir/frames/1.png"\n    }\n  ]\n}']
    ]);
  });

  it('--outputFrames < frameRate', async () => {
    const { spy, fn } = prep();
    const input = {
      outputDir: 'outputDir',
      frameRate: 2,
      outputFrames: { frameRate: 1 },
    };
    const { captureFrame, screenshotPromises, writeMetaData } = await fn(input);

    captureFrame({ screenshotCounter: 1, screenshot: 'screenshot1' });
    captureFrame({ screenshotCounter: 2, screenshot: 'screenshot2' });
    captureFrame({ screenshotCounter: 3, screenshot: 'screenshot3' });
    await screenshotPromises;
    await writeMetaData();
    const writeFileCallArgs = spy.writeFile.getCalls().map(c => c.args);
    assert.deepEqual(writeFileCallArgs, [
      ['outputDir/frames/0.png', 'screenshot1'],
      ['outputDir/frames/1.png', 'screenshot2'],
      ['outputDir/frames.json', '{\n  "frameRate": 2,\n  "frames": [\n    {\n      "path": "outputDir/frames/0.png"\n    },\n    {\n      "path": "outputDir/frames/1.png"\n    }\n  ]\n}']
    ]);
  });

  it('--outputFrames=scene', async () => {
    const { spy, fn } = prep();
    const input = {
      outputDir: 'outputDir',
      // frameRate: 2,
      outputFrames: { scene: true },
    };
    const { captureFrame, screenshotPromises, writeMetaData } = await fn(input);

    captureFrame({ screenshotCounter: 1, screenshot: 'screenshot1', scenes: [['scene', {}]] });
    captureFrame({ screenshotCounter: 2, screenshot: 'screenshot2', scenes: [['scene', {}]] });
    captureFrame({ screenshotCounter: 3, screenshot: 'screenshot3', scenes: [['scene', {}]] });
    await screenshotPromises;
    await writeMetaData();
    const writeFileCallArgs = spy.writeFile.getCalls().map(c => c.args);
    assert.deepEqual(writeFileCallArgs, [
      ['outputDir/frames/0.png', 'screenshot1'],
      ['outputDir/frames/1.png', 'screenshot2'],
      ['outputDir/frames.json', '{\n  "frameRate": 25,\n  "frames": [\n    {\n      "path": "outputDir/frames/0.png",\n      "scenes": [\n        [\n          "scene",\n          {}\n        ]\n      ]\n    },\n    {\n      "path": "outputDir/frames/1.png",\n      "scenes": [\n        [\n          "scene",\n          {}\n        ]\n      ]\n    }\n  ]\n}']
    ]);
  });

  it('--outputFrames=slide', async () => {
    const { spy, fn } = prep();
    const input = {
      outputDir: 'outputDir',
      // frameRate: 2,
      outputFrames: { slide: true },
    };
    const { captureFrame, screenshotPromises, writeMetaData } = await fn(input);

    captureFrame({ screenshotCounter: 1, screenshot: 'screenshot1', scenes: [['scene', {}]] });
    captureFrame({ screenshotCounter: 2, screenshot: 'screenshot2', scenes: [['scene', {}]] });
    captureFrame({ screenshotCounter: 3, screenshot: 'screenshot3', scenes: [['scene', {}]] });
    await screenshotPromises;
    await writeMetaData();
    const writeFileCallArgs = spy.writeFile.getCalls().map(c => c.args);
    assert.deepEqual(writeFileCallArgs, [
      ['outputDir/frames/0.png', 'screenshot1'],
      ['outputDir/frames/1.png', 'screenshot2'],
      ['outputDir/frames.json', '{\n  "frameRate": 25,\n  "frames": [\n    {\n      "path": "outputDir/frames/0.png",\n      "scenes": [\n        [\n          "scene",\n          {}\n        ]\n      ]\n    },\n    {\n      "path": "outputDir/frames/1.png",\n      "scenes": [\n        [\n          "scene",\n          {}\n        ]\n      ]\n    }\n  ]\n}']
    ]);
  });
});

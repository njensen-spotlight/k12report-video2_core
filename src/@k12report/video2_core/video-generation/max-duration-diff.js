const ffprobe = require('./ffprobe');

module.exports = async (input, { durationMaxDiff }) => {
  const probe = await ffprobe(input);
  let lastDuration;
  let durationDifference;
  for (const stream of probe.streams) {
    const { codec_type, duration: durationStr, ...rest } = stream;
    console.log(`Output stream '${codec_type}' duration: ${durationStr}`);
    const durationNum = parseFloat(durationStr);
    if (lastDuration && (durationDifference = Math.abs(durationNum - lastDuration)) > durationMaxDiff) {
      throw new Error(`Difference in duration among output streams larger than allowed (${durationDifference}s > ${durationMaxDiff}s)`);
    }
    lastDuration = durationNum;
    probe.sanitized[codec_type] = {
      ...rest,
      durationStr,
      duration: durationNum,
    }
  }
  if (durationDifference) {
    console.log(`Duration difference: ${durationDifference}`);
  }
  if (probe.streams.length < 2) {
    throw new Error(`Found ${2 - probe.streams.length} fewer streams in output, should've had at least 2`);
  }
};

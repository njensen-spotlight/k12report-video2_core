/**
 * Capture Audio
 *
 * Loads the web-app in a single puppeteer process,
 * lets it play out the complete audio till the end
 * then captures the end result
 */

const Path = require('path');
const fs = require('fs-extra');
const datauritobuffer = require('data-uri-to-buffer');
const Puppeteer = require('../puppeteer');

module.exports = async (config) => {
  const {
    webAppUrl,
    outputDir,
    debug,
    video: videoResult,
  } = config;

  const output = Path.join(outputDir, 'audio.tmp');
  if (config.cache && await fs.exists(output)) {
    return { output };
  }

  const beforeGoto = ({ page }) => {
    return page.evaluateOnNewDocument((videoResult) => {
      window.PUPPETEER_videoResult = videoResult;
    }, videoResult);
  };

  const { page } = await Puppeteer.puppeteer(webAppUrl, {
    ...config,
    label: 'audio',
    beforeGoto,
  });

  let audioData;
  let audioMetadata;
  let subtitles;
  try {
    audioData = await page.evaluate(async () => {
      await window.puppeteer.finishedPromise;
      return window.puppeteer.audioData;
    });
    audioMetadata = await page.evaluate(async () => {
      await window.puppeteer.finishedPromise;
      return window.puppeteer.audioMetadata;
    });
    subtitles = await page.evaluate(async () => {
      await window.puppeteer.finishedPromise;
      return window.puppeteer.subtitles;
    });
  } catch (error) {
    throw error;
  }

  try {
    let audioBuffer;
    try {
      audioBuffer = datauritobuffer(audioData);
    } catch (error) {}
    const promises = [];
    promises.push(fs.writeFile(output, audioBuffer || ''));
    if (subtitles) {
      if (subtitles.default) {
        promises.push(fs.writeFile(Path.join(outputDir, 'output.srt'), subtitles.default.srt));
        promises.push(fs.writeFile(Path.join(outputDir, 'output.vtt'), subtitles.default.vtt));
      }
      if (debug && subtitles.debug) {
        promises.push(fs.writeFile(Path.join(outputDir, 'output.debug.srt'), subtitles.debug.srt));
        promises.push(fs.writeFile(Path.join(outputDir, 'output.debug.vtt'), subtitles.debug.vtt));
      }
    }
    if (audioMetadata) {
      promises.push(fs.writeFile(Path.join(outputDir, 'audio.json'), JSON.stringify(audioMetadata, null, 2)));
    }
    await Promise.all(promises);
  } catch (error) {
    throw error;
  }

  return { output };
};

const proxyquire = require('proxyquire').noCallThru();
const assert = require('assert');
const sinon = require('sinon');
const t = require('../../tests/utils');

describe(__dirname, () => {

  const prep = (opts) => {
    const globals = {
      window: { puppeteer: {} }
    };
    const spy = {
      fs: {
        writeFile: sinon.spy(i => i),
      },
      'data-uri-to-buffer': sinon.spy(i => i),
      puppeteer: sinon.spy(() => ({ page: spy.page })),
      page: {
        evaluate: sinon.spy((e, ...args) => e(...args)),
        evaluateOnNewDocument: sinon.spy((e, ...args) => e(...args)),
      },
    };
    t.mockGlobals(globals);
    const fn = proxyquire('.', {
      'fs-extra': {
        exists: () => false,
        writeFile: spy.fs.writeFile,
        ...(opts && opts.proxyquire && opts.proxyquire['fs-extra']),
      },
      path: {
        join: (...paths) => paths.join('/'),
      },
      'data-uri-to-buffer': spy['data-uri-to-buffer'],
      '../puppeteer': { puppeteer: spy.puppeteer, }
    });
    return { spy, globals, fn };
  };

  it('basic', async () => {
    const { fn } = prep();
    const { output } = await fn({
      outputDir: 'outputDir',
    });
    assert(output);
  });

  it('cache', async () => {
    const { spy, fn } = prep({ proxyquire: { 'fs-extra': { exists: () => true } } });
    const { output } = await fn({
      outputDir: 'outputDir',
    });
    assert(output);
    assert(spy['data-uri-to-buffer'].notCalled);
    assert(spy.puppeteer.notCalled);
  });

  it('full', async () => {
    const { spy, globals, fn } = prep();
    globals.window.puppeteer.audioData = 'audioData';
    globals.window.puppeteer.subtitles = 'subtitles';
    const input = {
      outputDir: 'outputDir',
      webAppUrl: 'webAppUrl',
      video: 'video',
    };
    const { output } = await fn(input);
    assert(spy.puppeteer.calledOnceWith(input.webAppUrl));
    assert.equal(spy.puppeteer.getCalls()[0].args[1].outputDir, input.outputDir);
    assert(spy.puppeteer.getCalls()[0].args[1].beforeGoto);
    await spy.puppeteer.getCalls()[0].args[1].beforeGoto({ page: spy.page });
    assert(spy.page.evaluateOnNewDocument.calledOnce);
    assert.equal(spy.page.evaluateOnNewDocument.firstCall.args[1], input.video);
    await spy.page.evaluateOnNewDocument.firstCall.args[0]('PUPPETEER_videoResult');
    assert.equal(window.PUPPETEER_videoResult, 'PUPPETEER_videoResult');
    assert(spy.page.evaluate.calledThrice);
    assert(spy.fs.writeFile.calledOnceWith(output));
  });
});

const Path = require('path');

const adjust = {};

module.exports = (argv) => {
  for (const key in adjust) {
    argv[key] = adjust[key](argv[key], argv);
  }
  return argv;
};

adjust.outputFrames = (input) => {
  const output = {};
  if (typeof input === 'string') {
    const string = input;
    const split = string.split(',').map(s => s.trim());
    if (split.includes('slide')) {
      output.slide = true;
    } else if (split.includes('scene')) {
      output.scene = true;
    }
    if (split.includes('fill')) {
      output.fill = true;
    }
    let match;
    if (match = string.match(/^([0-9]+),/)) {
      output.frameRate = Number(match[1]);
    }
    if (match = string.match(/^(false|n)/)) {
      output.disable = true;
    }
  } else if (typeof input === 'number') {
    output.frameRate = input;
  } else if (input && input.toString) {
    return input;
  }
  if (output.frameRate == 0) {
    output.disable = true;
  }
  if (output.frameRate < 0) {
    throw new Error(`Invalid output.frameRate ${output.frameRate}`);
  }
  output.toString = () => input;
  // console.log({ input });
  // console.log({ output });
  // process.exit();
  return output;
};

adjust.outputDir = (path = 'output', { cwd = process.cwd() } = {}) => {
  if (!Path.isAbsolute(path)) {
    path = Path.resolve(cwd, path);
  }
  return path;
};

adjust.buildDir = (path, { cwd = process.cwd(), outputDir }) => {
  if (!path) {
    path = Path.join(outputDir, 'build');
  }
  if (!Path.isAbsolute(path)) {
    path = Path.resolve(cwd, path);
  }
  return path;
};

adjust.webAppUrl = (url, { buildDir }) => {
  url = `file:///${Path.join(buildDir, 'index.html')}`;
  return url;
};

const Path = require('path');
const CP = require('child_process');

const utils = module.exports = {};

const root = utils.root = (...paths) => Path.join(__dirname, '..', ...paths);
const cwd = utils.cwd = (...paths) => Path.join(process.cwd(), ...paths);
const pathSep = utils.pathSep = (process.platform === 'win32' ? ';' : ':');

const spawnerBase = spawner => (cmd, args = [], opts = {}) => spawner(cmd, [...args], {
  stdio: 'inherit',
  shell: true,
  // cwd: root,
  env: {
    // ...process.env,
    // ...argv,
    PATH: root('node_modules/.bin')
      + pathSep + cwd('node_modules/.bin')
      + pathSep + process.env.PATH,
    ...(opts && opts.env)
  },
  ...opts,
});

const spawnSync = utils.spawnSync = spawnerBase(CP.spawnSync);
const spawn = utils.spawn = spawnerBase(CP.spawn);

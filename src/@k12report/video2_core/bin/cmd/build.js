exports.command = 'build';
exports.description = 'Build the project (for video-generation)';
exports.handler = async argv => {
  const webpack = require('webpack-simple-node-api');
  const { build, watch } = webpack(await require('../../web-app/webpack.config.js')(argv));
  if (argv.watch) {
    return watch({ log: true });
  } else {
    return build({ log: true });
  }
};

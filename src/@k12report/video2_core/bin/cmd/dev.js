exports.command = 'dev';
exports.description = 'Run project in development mode';
exports.handler = async argv => {
  const webpack = require('webpack-simple-node-api');
  const { devServer } = webpack(await require('../../web-app/webpack.config.js')({ ...argv, isDev: true }));
  devServer();
};

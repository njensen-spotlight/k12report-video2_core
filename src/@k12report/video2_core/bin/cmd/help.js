exports.command = 'help';
exports.description = 'Show help';
exports.builder = yargs => yargs.showHelp();

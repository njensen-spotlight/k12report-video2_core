#!/usr/bin/env node

require('dotenv').config();
const yargs = require('yargs');
const options = require('./options');
const adjust = require('./adjust');
const config = require('../config');
const logToFile = require('../utils/log-to-file');
const { version } = require('../package.json');

let projectVersion;
try { projectVersion = require(process.cwd() + '/version'); } catch (error) {}

module.exports = yargs
  .scriptName('npx core')
  .options(options)
  .env()
  .middleware([
    adjust,
    argv => {
      try {
        const { metadata } = require(argv.webAppDir + '/data');
        if (metadata.config) {
          console.log('Applying metadata.config');
          Object.assign(argv, metadata.config);
        }
      } catch (error) {
        console.warn('Cannot read `metadata.config`.', error.message);
      }
    },
    argv => {
      Object.assign(config, argv);
    },
    argv => {
      if (argv.logToFile) {
        logToFile.patch(argv.outputDir);
      }
    },
    argv => {
      console.log(
        `core (v${version})`,
        ...argv._,
        ...Object.entries(options).map(([o]) => String(argv[o]) !== String(options[o].default) && `--${o}=${argv[o]}`).filter(Boolean)
      );
      if (projectVersion) {
        console.log('Project running at version:', projectVersion.version);
      } else {
        console.warn('WARNING: Couldn\'t find project \'version.json\'');
      }
    }
  ])
  .commandDir(__dirname + '/cmd')
  .wrap(null)
  .argv;

const Path = require('path');

const cwd = (...path) => Path.join(process.cwd(), ...path);
const core = (...path) => Path.join(__dirname, '../', ...path);

const options = exports;

options.webAppDir = {
  type: 'string',
  default: cwd('web-app'),
  description: 'web-app dir',
};
options.outputDir = {
  alias: ['o'],
  type: 'string',
  default: cwd('output'),
  description: 'Video output directory',
};
options.buildDir = {
  type: 'string',
  default: cwd('output/build'),
  description: 'Dir where web-app is built',
};
options.webpackConfig = {
  type: 'string',
  default: core('webpack.config.js'),
  description: 'Webpack Config to use',
};
options.webAppUrl = {
  type: 'string',
  default: `file:///${cwd('output/build/index.html')}`,
  description: 'Web App URL',
};
options.headless = {
  type: 'boolean',
  default: true,
  description: 'Hide the Browser Window while capturing',
};
options.devtools = {
  type: 'boolean',
  // default: false,
  description: 'Show the Browser Window and auto-open DevTools panel while capturing',
};
options.port = {
  type: 'number',
  default: 8000,
  description: 'Port for dev',
};
options.concurrency = {
  alias: ['c'],
  type: 'number',
  default: 2,
  description: 'Simultaneous capturing of audio/video(s)',
};
options.verbose = {
  alias: ['v'],
  // type: 'level',
  // default: false,
  description: 'Enables/increases verbose logging',
};
options.debug = {
  alias: ['d'],
  // type: 'level',
  // default: false,
  description: 'Enables/increases the debug level',
};
options.timeDiff = {
  alias: ['timediff'],
  description: 'Show TimeDiff, to debug/measure time between events',
};
options.throttledLogDelay = {
  type: 'number',
  default: 3 * 60,
  description: 'Delay between throttled logs (secs)',
};
options.tempContextSwitch = {
  type: 'boolean',
};
options.cleanup = {
  type: 'boolean',
  default: true,
  description: 'Cleans up temporary files',
};
options.waitBetweenScreenshots = {
  type: 'number',
  default: 10,
  description: 'Wait for a bit every N screenshots',
};
options.waitBetweenScreenshotsFor = {
  type: 'number',
  default: 100,
  description: 'Wait for N msecs every few screenshots',
};
options.probe = {
  type: 'boolean',
  default: true,
  description: 'ffprobe info for input/output files',
};
options.maxDurationDiff = {
  type: 'number',
  default: 30,
  description: 'Difference in duration (in seconds) among output streams greater would throw',
};
options.maxSilenceStart = {
  type: 'number',
  default: 1,
  description: 'Silence in the audio at the beginning (in seconds) allowed',
};
options.silenceThreshold = {
  type: 'number',
  default: 0.02,
  description: 'Silence threshold to use when detecting silence',
};
options.logToFile = {
  type: 'boolean',
  description: 'Tees logs to a .log file',
};
options.frameRate = {
  type: 'number',
  alias: ['framerate'],
  description: 'Framerate at which to capture (overrides metadata)',
};
options.outputFrames = {
  default: 1,
  description: 'Whether or not to output frames data, and how often [falsey|Number(1-Infinity)|String(slide|scene)[,fill]]',
};
options.webm = {
  type: 'boolean',
  // default: false,
  description: 'Convert MP3/MP4 to WEBM',
};
options.localChrome = {
  type: 'boolean',
  default: true,
  description: 'Use local chrome (instead of node_modules/chromium)',
};
options.chromePath = {
  type: 'string',
  description: 'Use specified chrome/chromium path',
};
options.screencast = {
  type: 'boolean',
  // default: false,
  description: 'Use screen cast to capture video',
};
options.adjustLength = {
  description: `Adjust audio/video length.
    "=audio" Stretch/compress audio to fit video length. (i.e. adjusts the audio, keeps the video the same)
    "=video" vice-versa
    "=long" Stretch whichever is shorter. I.e. make it long
    "=short" Compress whichever is longer. I.e. make it short
  `,
};
options.cache = {
  type: 'boolean',
  description: 'Store and use cache where possible',
};
options.audioOnly = {
  type: 'boolean',
  description: 'Capture only the audio',
};
options.combine = {
  type: 'boolean',
  default: true,
  description: 'Combine the captured audio/video',
};
